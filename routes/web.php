<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['prefix' => 'superadmin', 'middleware' => 'auth'], function(){
	Route::get('/', 'MainController@index')->name('superadmin');
	Route::get('/categories', 'MainController@categories')->name('superadmin.categories');
	Route::get('/category/{id}', 'MainController@show_category');
});


Route::post('cart/ajax', 'FrontController@ajax_cart_update');
Route::post('wishlist/ajax', 'FrontController@ajax_wishlist_update');

Route::group(['prefix' => 'theme5'], function(){

	Route::get('/', 'Theme5\FrontController@index');
	Route::get('category/{slug}', 'Theme5\FrontController@get_products_by_category')->name('category.product.new5');
	Route::get('product/{slug}', 'Theme5\FrontController@view_product')->name('view.product.new5');
	Route::get('contact-us', 'Theme5\FrontController@contact');
	Route::get('privacy-policy', 'Theme5\FrontController@privacy_policy');
	Route::get('show-cart', 'Theme5\FrontController@cart_view')->name('cart.view5');
	Route::get('wish-list/{id}', 'Theme5\FrontController@wishlist')->middleware(['auth'])->name('wish5');
	Route::get('show-checkout', 'Theme5\FrontController@checkout_view')->name('checkout.view5');
	Route::post('search/product', 'Theme5\FrontController@search_product')->name('search.product5');
	Route::get('about-us', 'Theme5\FrontController@about_us');

});

Route::group(['prefix' => 'theme4'], function(){

	Route::get('/', 'Theme4\FrontController@index');
	Route::get('category/{slug}', 'Theme4\FrontController@get_products_by_category')->name('category.product.new4');
	Route::get('product/{slug}', 'Theme4\FrontController@view_product')->name('view.product.new4');
	Route::get('contact-us', 'Theme4\FrontController@contact');
	Route::get('privacy-policy', 'Theme4\FrontController@privacy_policy');
	Route::get('show-cart', 'Theme4\FrontController@cart_view')->name('cart.view4');
	Route::get('wish-list/{id}', 'Theme4\FrontController@wishlist')->middleware(['auth'])->name('wish4');
	Route::get('show-checkout', 'Theme4\FrontController@checkout_view')->name('checkout.view4');
	Route::post('search/product', 'Theme4\FrontController@search_product')->name('search.product4');
	Route::get('about-us', 'Theme4\FrontController@about_us');

});

Route::group(['prefix' => 'theme3'], function(){

	Route::get('/', 'Theme3\FrontController@index');
	Route::get('category/{slug}', 'Theme3\FrontController@get_products_by_category')->name('category.product.new3');
	Route::get('product/{slug}', 'Theme3\FrontController@view_product')->name('view.product.new3');
	Route::get('contact-us', 'Theme3\FrontController@contact');
	Route::get('privacy-policy', 'Theme3\FrontController@privacy_policy');
	Route::get('show-cart', 'Theme3\FrontController@cart_view')->name('cart.view3');
	Route::get('wish-list/{id}', 'Theme3\FrontController@wishlist')->middleware(['auth'])->name('wish3');
	Route::get('show-checkout', 'Theme3\FrontController@checkout_view')->name('checkout.view3');
	Route::post('search/product', 'Theme3\FrontController@search_product')->name('search.product3');

});

Route::group(['prefix' => 'theme2'], function(){

	Route::get('/', 'Theme2\FrontController@index');
	Route::get('privacy-policy', 'Theme2\FrontController@privacy_policy');
	Route::get('contact-us', 'Theme2\FrontController@contact');
	Route::get('more-products', 'Theme2\FrontController@get_more_products_new')->name('more.products.new2');
	Route::get('category/{slug}', 'Theme2\FrontController@get_products_by_category_new')->name('category.product.new2');
	Route::get('product/{slug}', 'Theme2\FrontController@view_product_new')->name('view.product.new2');
	// Route::resource('cart', 'Theme2\CartController');
	// Route::resource('checkout', 'Theme2\CheckoutController');
	Route::get('show-cart', 'Theme2\FrontController@cart_view')->name('cart.view2');
	Route::get('show-checkout', 'Theme2\FrontController@checkout_view')->name('checkout.view2');
	Route::get('login', 'Theme2\FrontController@login_new')->name('login.new2');
	Route::get('wish-list/{id}', 'Theme2\FrontController@wishlist')->middleware(['auth'])->name('wish2');
	Route::get('add-to-wish-list/{id}', 'Theme2\FrontController@wishlist_add')->middleware(['auth'])->name('wishlist.add2');
	Route::get('tag/{slug}', 'Theme2\FrontController@products_by_tag')->name('tag.products2');
	Route::post('search/product', 'Theme2\FrontController@search_product')->name('search.product2');
});

Route::group(['prefix' => 'theme1'], function(){

	Route::get('/', 'FrontController@index_new');
	Route::get('privacy-policy', 'FrontController@privacy_policy');
	Route::get('more-products', 'FrontController@get_more_products_new')->name('more.products.new');
	Route::get('category/{slug}', 'FrontController@get_products_by_category_new')->name('category.product.new');
	Route::get('product/{slug}', 'FrontController@view_product_new')->name('view.product.new');
	// Route::resource('cart', 'CartNewController');
	// Route::resource('checkout', 'CheckoutNewController');
	Route::get('show-cart', 'FrontController@cart_view')->name('cart.view');
	Route::get('show-checkout', 'FrontController@checkout_view')->name('checkout.view');
	Route::get('login', 'FrontController@login_new')->name('login.new');
	Route::get('wish-list/{id}', 'FrontController@wishlist')->middleware(['auth']);
	Route::get('add-to-wish-list/{id}', 'FrontController@wishlist_add')->middleware(['auth'])->name('wishlist.add');
	Route::get('tag/{slug}', 'FrontController@products_by_tag')->name('tag.products');
});

Route::get('/', 'FrontController@index_new');
Route::get('more-products', 'FrontController@get_more_products')->name('more.products');
Route::get('category/{slug}', 'FrontController@get_products_by_category')->name('category.product');
Route::get('product/{slug}', 'FrontController@view_product')->name('view.product');
Route::resource('cart', 'CartController');
Route::resource('checkout', 'CheckoutController');

Auth::routes();
Route::get('/order', function(){
	return view('boost_order');
});
Route::post('/boost-order/store', 'FrontController@boost_order_store')->name('boost.order.store'); 
Route::get('/', 'FrontController@index')->name('index');
Route::get('admin/boosted/orders', 'OrdersController@boosted_orders')->name('orders.boosted_orders');

Route::get('/suppliers', 'FrontController@get_all_suppliers')->name('suppliers');
Route::get('/privacy_policy', function(){
	return view('pages.privacy_policy');
});

Route::get('register', function(){
	return view('auth.login');
})->name('register');
Route::get('supplier', function(){
	return view('pages.suppliers.index')->with('users', App\User::orderBy('id','desc')->get());
});
Route::get('supplier/{id}', function(){
	return view('pages.suppliers.index')->with('users', App\User::orderBy('id','desc')->get());
});
Route::get('category', function(){
	return redirect()->route('index');
});
Route::get('product', function(){
	return redirect()->route('index');
});

Route::get('supplier/{id}/{name}', 'FrontController@view_supplier')->name('view.supplier');
Route::post('search/product', 'FrontController@search_product')->name('search.product');
Route::post('user_reviews', 'FrontController@add_review')->name('add.review');
Route::post('product/review', 'FrontController@insert_product_review')->name('product.review');
Route::post('product/by/location', 'FrontController@get_products_by_location')->name('product.location');

Route::get('Featured-Products', 'FrontController@get_featured_products')->name('featured.products');

//ORDERS
Route::post('/order/store', 'OrdersController@store_order')->name('order.store');

Route::get('/ajax/set_session', 'FrontController@set_session_product_id');

Route::group([
				'prefix' => 'admin',
				'middleware' => 'auth',
			],
			function(){
				Route::get('/', 'ProductController@index')->name('dashboard');
				Route::resource('/products', 'ProductController');
				Route::resource('/categories', 'CategoryController');
				Route::resource('/profile', 'ProfileController');
				Route::resource('/users', 'UserController');
				Route::resource('/photos', 'PhotosController');
        		Route::resource('/videos', 'VideosController');
				Route::post('/make/featured', 'AdminController@make_featured')->name('make.featured');
				Route::post('/unmake/featured', 'AdminController@unmake_featured')->name('unmake.featured');
		        Route::get('featured', 'AdminController@featured')->name('featured.index');
		        Route::resource('orders', 'OrdersController');
		        Route::resource('reviews', 'ReviewsController');
		        Route::get('/showorders', 'AdminController@get_all_orders')->name('orders.showall');
		        Route::get('suspend/users', 'AdminController@suspend_users')->name('users.suspend');
		        Route::post('suspend/users', 'AdminController@make_user_suspend')->name('make.users.suspend');
		        Route::resource('/managesuppliers', 'SupplierController', ['except' => ['show', 'index', 'destroy']]);
		        Route::resource('sliders', 'SlidersController');
		        Route::resource('tags', 'TagsController');
		        Route::get('product/removeImage/{id}', 'ProductController@remove_image')->name('remove.image');
		        Route::resource('sitesettings', 'SettingsController');
			});

Route::get('login/facebook/', 'Auth\LoginController@redirectToFacebookProvider')->name('facebook.login');
Route::get('login/facebook/callback', 'Auth\LoginController@handleProviderCallback');
