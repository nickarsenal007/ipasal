<?php

namespace App\Http\Controllers\Theme4;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Product;
use App\Setting;
use App\Category;
use App\User;

use Session;

class FrontController extends Controller
{
	
	public function index() {

		$products = Product::notsuspended()
                          ->orderBy('products.id','desc')
                          ->where('featured', 0);

        $featured_products = Product::notsuspended()
                          ->where('products.featured',1)
                          ->orderBy('products.updated_at','desc')->take(6)->get();

        $setting = Setting::first();

      	return view('theme4.index')->with('products', $products)
                                ->with('featured_products', $featured_products);

	}

	public function get_products_by_category($slug){

        $id_get = Category::where('slug', $slug)->first();
        if( !$id_get ){
            return redirect()->route('index');
        }
        $id = $id_get->id;

        if (request()->sort == 'low_high') {
            $products = Category::find($id)->products()->orderBy('rate')->paginate(9);
        }elseif(request()->sort == 'high_low'){
            $products = Category::find($id)->products()->orderBy('rate', 'desc')->paginate(9);
        }elseif(request()->rating == 'low_high'){
            $products = Category::find($id)->products()
                ->with(['ratings' => function ($query){
                            $query->orderBy('rating');
                        }])->paginate(9);
        }elseif(request()->rating == 'high_low'){
            $products = Category::find($id)->products()
                ->with(['ratings' => function ($query){
                            $query->orderBy('rating', 'desc');
                        }])->paginate(9);
        }
        else{
            $products = Category::find($id)->products()->orderBy('productName')->paginate(9);
        }
        $catname = Category::find($id);
        $parentcategories = Category::where('parentId', 0)->get()->take(10);
        $title = 'Category: ' . $catname->name;
        return view('theme4.products')->with([
                                                'products' => $products,
                                                'categories' => Category::all(),
                                                'catname' => $catname,
                                                'title' => $title,
                                                'parentcategories' => $parentcategories
                                            ]);

    }

    public function view_product( $slug ) {

        $product = Product::where('slug', $slug)->firstOrFail();
        if (empty($product)) {
            return redirect()->route('index');
        }
        $cat_id = $product->categoryId;
        $category = Category::find($cat_id);
        $ratings = $product->ratings()->select('rating')->get();
        $averageRating = collect($ratings)->avg('rating');

        $productsOfCategory = '';
        $productReviews = '';
        $productsOfCategory = Product::latest()->where('categoryId', $cat_id)->where('id', '!=', $product->id)->get()->take(4);
        $productReviews = $product->reviews()->orderBy('created_at')->get();

        $relatedproducts = Product::where('categoryId', $cat_id)->where('id', '!=', $product->id)->inRandomOrder()->get()->take(4);
        
        return view('theme4.single')->with([
                                                'product'=> $product,
                                                'productsOfCategory' => $productsOfCategory,
                                                'productReviews' => $productReviews,
                                                'averageRating' => $averageRating,
                                                'category' => $category,
                                                'relatedproducts' => $relatedproducts
                                                ]);
    }

    public function wishlist($userId)
    {
        $user = User::find($userId);
        $wishlist = $user->wishlists()->where('deleted', 0)->get()->pluck('productId')->toArray();
        
        $products = Product::wherein('id', $wishlist)->paginate(9);

        $title = 'Your Wish List';
        return view('theme4.wishlist', compact('products', 'title'));
    }

    public function search_product(Request $request) {

        $this->validateWith([
            'search' => 'required',
        ]);

        $products = Product::where('productName', 'like', '%'.$request->search.'%')->paginate(9);

        $title = 'Search results for "' . $request->search . '"';
        return view('theme4.wishlist', compact('products', 'title'));

    }

    public function contact() {
        return view('theme4.contact');
    }

    public function privacy_policy() {
        return view('theme4.privacypolicy');
    }

    public function cart_view() {
        return view('theme4.cart');
    }

    public function checkout_view() {
        return view('theme4.checkout');
    }

    public function about_us() {
        return view('theme4.aboutus');
    }
	
}