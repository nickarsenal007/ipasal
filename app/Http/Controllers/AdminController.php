<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Product;
use App\Orders;
use App\User;

class AdminController extends Controller
{
    public function __construct(){

        $this->middleware('auth');
    }

    public function featured(){

        return view('admin.featured');
    }

    public function unmake_featured(){

      Product::where('id', request()->notfeatured )->update([ 'featured' => 0 ]);
      session()->flash('success', 'Success');
      return redirect()->back();
    }

    public function make_featured(Request $request){

    	$this->validateWith([
    			'featured' => 'required',
    	]);

    	Product::where('id', $request->featured )->update([ 'featured' => 1 ]);
        
        session()->flash('success', 'Success');
        return redirect()->back();
    }

    public function get_all_orders(){

      $orders = Orders::orderBy('id', 'desc')->with('product')->get();
      return view('admin.orders')->with('orders', $orders);
    }

    public function suspend_users(){
      $user = User::all();

      return view('admin.suspend_users')->with('users', $user);
    }

    public function make_user_suspend(Request $request){

      if (empty($request->user_ids)) {

        User::where('suspend', '1')->update(['suspend' => 0]);

        session()->flash('success', 'Succesfull');
        return redirect()->back();
      }

      User::whereIn('id', $request->user_ids )->update([ 'suspend' => 1 ]);
      User::whereNotIn('id', $request->user_ids )->update([ 'suspend' => 0 ]);

      session()->flash('success', 'Succesfull');
      return redirect()->back();

    }

}
