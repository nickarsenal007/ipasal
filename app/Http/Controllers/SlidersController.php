<?php

namespace App\Http\Controllers;

use App\Sliders;
use App\Category;
use Session;
use Image;
use Illuminate\Http\Request;

class SlidersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $sliders = Sliders::latest()->get();
        return view('sliders.index', compact('sliders'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories = Category::orderBy('name')->get();
        return view('sliders.create', compact('categories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
        $this->validateWith([
            'textMain' => 'required',
            'category' => 'required',
            'sliderImage' => 'required|image'
        ]);

        $categoryname = Category::find($request->category);
        $categoryname = $categoryname->name; 

        $file = $request->file('sliderImage');
        if ( $file ) {
            $filename = 'slider-' . str_slug( $request->textMain ) . '-' . str_random(10) . '.' . $file->getClientOriginalExtension();

            Image::make($file)->resize(600, 450)->save('uploads/sliders/resized/'. $filename);
            Image::make($file)->save('uploads/sliders/' . $filename);
        }

        $slider = new Sliders;

        $slider->textMain = $request->textMain;
        $slider->textSecondary = $request->textSecondary;
        $slider->sliderImage = $filename;
        $slider->categoryId = $request->category;
        $slider->categoryName = $categoryname;
        $slider->showSlider = 1;

        $slider->save();

        Session::flash('success', 'Succesfully added a slider.');

        return redirect()->route('sliders.index');

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Sliders  $sliders
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $slider = Sliders::findOrFail($id);
        $categories = Category::orderBy('name')->get();

        return view('sliders.edit', compact('slider', 'categories'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Sliders  $sliders
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validateWith([
            'textMain' => 'required',
            'category' => 'required',
            'sliderImage' => 'image'
        ]);

        $slider = Sliders::findOrFail($id);

        $categoryname = Category::find($request->category);
        $categoryname = $categoryname->name; 

        $file = $request->file('sliderImage');
        $filename = $slider->sliderImage;
        if ( $file ) {
            $filename = 'slider-' . str_slug( $request->textMain ) . '-' . str_random(10) . '.' . $file->getClientOriginalExtension();

            if ( file_exists('uploads/sliders/' . $slider->sliderImage) ) {
                unlink('uploads/sliders/' . $slider->sliderImage);
            }

            Image::make($file)->resize(600, 450)->save('uploads/sliders/resized/'. $filename);
            Image::make($file)->save('uploads/sliders/' . $filename);
        }

        

        $slider->textMain = $request->textMain;
        $slider->textSecondary = $request->textSecondary;
        $slider->sliderImage = $filename;
        $slider->categoryId = $request->category;
        $slider->categoryName = $categoryname;
        $slider->showSlider = $slider->showSlider;

        $slider->save();

        Session::flash('success', 'Succesfully added a slider.');

        return redirect()->route('sliders.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Sliders  $sliders
     * @return \Illuminate\Http\Response
     */
    public function destroy(Sliders $sliders)
    {
        //
    }
}
