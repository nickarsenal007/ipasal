<ul class="list-group">
    <li class="list-group-item"><a href="{{ route('dashboard') }}"><h4>Dashboard</h4></a></li>
    @php
        $uid = Auth::user();
    @endphp
    @if (!$uid->supplier)
        <li class="list-group-item">
            <a href="{{ route('managesuppliers.create') }}">Add Profile Details</a><br>
            <small style="color: darkred">Please Add your Profile to feature in the site.</small>
        </li>
    @else
        <li class="list-group-item">
            <a href="{{ route('managesuppliers.edit', $uid->id) }}">Update Profile</a>
        </li>
    @endif
    <li class="list-group-item">
        <a href="{{ route('products.create') }}">Add a Product</a>
    </li>
    <li class="list-group-item">
        <a href="{{ route('products.index') }}">Manage Products</a>
    </li>
   <!--  <li class="list-group-item">
        <a href="{{ route('videos.index') }}">Manage Your Videos</a>
    </li> -->
    <li class="list-group-item">
        <a href="{{ route('orders.index') }}">User Orders</a>
    </li>

</ul>
