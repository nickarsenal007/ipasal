@extends('theme5.layouts.main')

@section('content')

<section id="cart_items">
		<div class="container">
			<div class="breadcrumbs">
				<ol class="breadcrumb">
				  <li><a href="{{ url('theme5') }}">Home</a></li>
				  <li class="active">Shopping Cart</li>
				</ol>
			</div>
			<div class="table-responsive cart_info">
				<table class="table table-condensed">
					<thead>
						<tr class="cart_menu">
							<td class="image">Item</td>
							<td class="description"></td>
							<td class="price">Price</td>
							<!-- <td class="quantity">Quantity</td> -->
							<td class="total">Total</td>
							<td>Remove</td>
						</tr>
					</thead>
					<tbody>
					@if( count( Cart::content() ) )
                        @foreach( Cart::content() as $item )
						<tr>
							<td class="cart_product">
								<a href=""><img width="50%" height="160" src="{{ asset('uploads/products/thumbnails/'.$item->model->featuredImage) }}" alt=""></a>
							</td>
							<td class="cart_description">
								<h4><a href="{{ route('view.product.new5', $item->model->slug) }}">{{ $item->model->productName }}</a></h4>
							</td>
							<td class="cart_price">
								<p>{{ $item->model->rate }}/-</p>
							</td>
							<!-- <td class="cart_quantity">
								<div class="cart_quantity_button">
									<a class="cart_quantity_up" href=""> + </a>
									<input class="cart_quantity_input" type="text" name="quantity" value="1" autocomplete="off" size="2">
									<a class="cart_quantity_down" href=""> - </a>
								</div>
							</td> -->
							<td class="cart_total">
								<p class="cart_total_price">NRs. {{ $item->model->rate }}</p>
							</td>
							<td class="cart_delete">
								<a class="cart_quantity_delete" href="" onclick="event.preventDefault();document.getElementById('remove-item-{{$item->model->slug}}').submit();"><i class="fa fa-times"></i></a>
								<form id="remove-item-{{$item->model->slug}}" action="{{ route('cart.destroy', $item->rowId) }}" method="post">
                                    {{csrf_field()}}
                                    {{ method_field('delete') }}
                                </form>
							</td>
						</tr>
						@endforeach
					@else
						<tr>
							<td colspan="5"><h4>&nbsp;0 items.</h4></td>
						</tr>
					@endif
					</tbody>
				</table>
			</div>
		</div>
	</section> <!--/#cart_items-->

	<section id="do_action">
		<div class="container">
			<div class="heading">
				<h3>What would you like to do next?</h3>
				<p>Choose if you have a discount code or reward points you want to use or would like to estimate your delivery cost.</p>
			</div>
			<div class="row">
				<div class="col-sm-6">
					<div class="chose_area">
						<ul class="user_option">
							<li>
								<input type="checkbox">
								<label>Use Coupon Code</label>
							</li>
							<li>
								<input type="checkbox" checked>
								<label>Free Delivery</label>
							</li>
						</ul>
						<ul class="user_info">
							<li class="single_field zip-field">
								<label>Coupon Code:</label>
								<input type="text" name="couponcode">
							</li>
						</ul>
					</div>
				</div>
				<div class="col-sm-6">
					<div class="total_area">
						<ul>
							<li>Cart Sub Total <span>{{Cart::subtotal()}}</span></li>
							<li>Shipping Cost <span>Free</span></li>
							<li>Total <span>NRs. {{Cart::subtotal()}}/-</span></li>
						</ul>
							<a class="btn btn-default update" href="">Update</a>
							<a class="btn btn-default check_out" href="{{ route('checkout.view5') }}">Check Out</a>
					</div>
				</div>
			</div>
		</div>
	</section><!--/#do_action-->

@endsection