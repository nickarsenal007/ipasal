<section>
    <div class="container">
        <div class="row">
            <?php $categories = App\Category::where('parentId', 0)->get()->take(4); ?>
            <div class="col-sm-3">
                <div class="left-sidebar">
                    <h2>Category</h2>
                    <div class="panel-group category-products" id="accordian">
                        <!--category-productsr-->
                        @foreach($categories as $category)
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4 class="panel-title">
                                    <a data-toggle="collapse" data-parent="#accordian" href="#{{ $category->slug }}">
                                        @if( $category->children() )
                                        <span class="badge pull-right"><i class="fa fa-plus"></i></span>
                                        @endif
                                        {{ $category->name }}
                                    </a>
                                </h4>
                            </div>
                            @if( $category->children() )
                            <div id="{{ $category->slug }}" class="panel-collapse collapse">
                                <div class="panel-body">
                                    <ul>
                                        @foreach( $category->children()->take(4) as $child)
                                        <li><a href="{{ route('category.product.new5', $child->slug) }}">{{ $child->name }} </a></li>
                                        @endforeach
                                    </ul>
                                </div>
                            </div>
                            @endif
                        </div>
                        @endforeach
                    </div>
                    <!--/category-products-->

                    <div class="shipping text-center">
                        <!--shipping-->
                        <a href="{{ url('theme5') }}"><img src="{{ asset('themes/5/images/home/shipping.jpg') }}" alt="" /></a>
                    </div>
                    <!--/shipping-->

                </div>
            </div>

            <div class="col-sm-9 padding-right">
                <?php $featuredproducts = App\Product::notsuspended()->where('featured', 1)->inRandomOrder()->get()->take(9); ?>
                @if( count($featuredproducts) )
                <div class="features_items">
                    <!--features_items-->
                    <h2 class="title text-center">Featured Products</h2>
                    @foreach($featuredproducts as $product)
                    <div class="col-sm-4">
                        <div class="product-image-wrapper">
                            <div class="single-products">
                                <div class="productinfo text-center">
                                    <img src="{{ asset('uploads/products/thumbnails/' . $product->featuredImage) }}" alt="" />
                                    <h2><b>NRs. {{ $product->rate }}</b></h2>
                                    <p>{{ $product->productName }}</p>
                                    <a href="#" class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i>Add to cart</a>
                                </div>
                                <div class="product-overlay">
                                    <div class="overlay-content">
                                        <a href="{{ route('view.product.new5', $product->slug) }}">
                                            <h2><b>{{ $product->productName }}</b></h2>
                                        </a>
                                        <p><b>NRs. {{ $product->rate }}</b></p>
                                        <a href="#" class="btn btn-default add-to-cart" data-productid="{{$product->id}}" data-product="{{ $product->productName }}" data-rate="{{ $product->rate }}"><i class="fa fa-shopping-cart"></i>Add to cart</a>
                                    </div>
                                </div>
                            </div>
                            <div class="choose">
                                <ul class="nav nav-pills nav-justified">
                                    <li><a href="#" class="add-to-wishlist" data-product="{{ $product->id }}"><i class="fa fa-plus-square"></i>Add to wishlist</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    @endforeach
                </div>
                <!--features_items-->
                @endif
                <div class="category-tab">
                    <?php $featuredCategories = App\Category::where('featured', 1)->inRandomOrder()->get()->take(4); ?>
                    <?php $active = 'active'; ?>
                    <!--category-tab-->
                    <div class="col-sm-12">
                        <ul class="nav nav-tabs">
                            @foreach($featuredCategories as $category)
                            <li class="{{ $active }}"><a href="#{{ $category->slug }}" data-toggle="tab">{{ $category->name }}</a></li>
                            <?php $active = ''; ?>
                            @endforeach
                        </ul>
                    </div>
                    <div class="tab-content">
                        <?php $active = 'active in'; ?>
                        @foreach($featuredCategories as $category)
                        <?php $products = $category->products()->inRandomOrder()->get()->take(8); ?>
                        <div class="tab-pane fade {{ $active }}" id="{{ $category->slug }}">
                            @foreach($products as $product)
                            <div class="col-sm-3">
                                <div class="product-image-wrapper">
                                    <div class="single-products">
                                        <div class="productinfo text-center">
                                            <img src="{{ asset('uploads/products/thumbnails/' . $product->featuredImage) }}" alt="" />
                                            <h2><b>NRs. {{ $product->rate }}</b></h2>
                                            <p>{{ $product->productName }}</p>
                                            <a href="#" class="btn btn-default add-to-cart" data-productid="{{$product->id}}" data-product="{{ $product->productName }}" data-rate="{{ $product->rate }}"><i class="fa fa-shopping-cart"></i>Add to cart</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            @endforeach
                        </div>
                        <?php $active = ''; ?>
                        @endforeach
                    </div>
                    </?>
                    <!--/category-tab-->
                </div>
            </div>
        </div>
</section>