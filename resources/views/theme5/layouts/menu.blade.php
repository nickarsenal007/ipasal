<div class="header-middle">
    <!--header-middle-->
    <div class="container">
        <div class="row">
            <div class="col-sm-4">
                <div class="logo pull-left">
                    <a href="{{ url('theme5') }}"><img src="{{ asset('themes/5/images/home/logo.png') }}" alt="" /></a>
                </div>
                <div class="btn-group pull-right">
                    <div class="btn-group">
                        <button type="button" class="btn btn-default dropdown-toggle usa" data-toggle="dropdown">
                            Nepal
                            <span class="caret"></span>
                        </button>
                    </div>

                    <div class="btn-group">
                        <button type="button" class="btn btn-default dropdown-toggle usa" data-toggle="dropdown">
                            NRs.
                            <span class="caret"></span>
                        </button>
                    </div>
                </div>
            </div>
            <div class="col-sm-8">
                <div class="shop-menu pull-right">
                    <ul class="nav navbar-nav">
                        @auth
                        <li><a href="{{ route('wish5', Auth::id()) }}"><i class="fa fa-star"></i> Wishlist</a></li>
                        @endauth
                        <li><a href="{{ route('checkout.view5') }}"><i class="fa fa-crosshairs"></i> Checkout</a></li>
                        <li><a href="{{ route('cart.view5') }}"><i class="fa fa-shopping-cart"></i> Cart <span class="badge">{{ Cart::content()->count() }}</span></a></li>
                        @guest
                        <li><a href="{{ route('login') }}"><i class="fa fa-lock"></i> Login</a></li>
                        @endguest
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
<!--/header-middle-->

<div class="header-bottom">
    <!--header-bottom-->
    <div class="container">
        <div class="row">
            <div class="col-sm-9">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                </div>
                <div class="mainmenu pull-left">
                    <ul class="nav navbar-nav collapse navbar-collapse">
                        <li><a href="{{ url('theme5') }}" class="active">Home</a></li>
                        <li class="dropdown"><a href="#">Shop<i class="fa fa-angle-down"></i></a>
                            <ul role="menu" class="sub-menu">
                                <li><a href="">Featured</a></li>
                            </ul>
                        </li>
                        <li><a href="{{ url('theme5/contact-us') }}">Contact Us</a></li>
                    </ul>
                </div>
            </div>
            <div class="col-sm-3">
                <div class="search_box pull-right">
                    <form action="{{ route('search.product5') }}" method="post">
                        {{ csrf_field() }}
                        <input type="text" name="search" placeholder="Search" />
                        <noscript>Submit</noscript>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<!--/header-bottom-->