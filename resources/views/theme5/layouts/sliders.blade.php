<?php $slides = App\Sliders::inRandomOrder()->get()->take(3); ?>
@if( count($slides) )
<section id="slider">
    <!--slider-->
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <div id="slider-carousel" class="carousel slide" data-ride="carousel">
                    @if( count($slides) > 1 )
                    <?php $active = 'active';
                    $i = 0; ?>
                    <ol class="carousel-indicators">
                        @foreach($slides as $slide)
                        <li data-target="#slider-carousel" data-slide-to="{{ $i }}" class="{{ $active }}"></li>
                        <?php $active = '';
                        $i++; ?>
                        @endforeach
                    </ol>
                    @endif
                    <?php $active = 'active'; ?>
                    <div class="carousel-inner">
                        @foreach($slides as $slide)
                        <div class="item {{ $active }}">
                            <?php $active = ''; ?>
                            <div class="col-sm-6">
                                <h1>{!! $slide->textMain !!}</h1>
                                <h2>{!! $slide->textSecondary !!}</h2>
                                <a class="btn hvr-hover" href="{{ route('category.product.new4', str_slug($slide->categoryName)) }}">
                                    <button type="button" class="btn btn-default get">Get it now</button>
                                </a>
                            </div>
                            <div class="col-sm-6">
                                <img width="100%" src="{{ 'uploads/sliders/' . $slide->sliderImage }}" style="height:370px;object-fit:cover" class="girl img-responsive" alt="" />
                            </div>
                        </div>
                        @endforeach
                    </div>
                    @if( count($slides) > 1 )
                    <a href="#slider-carousel" class="left control-carousel hidden-xs" data-slide="prev">
                        <i class="fa fa-angle-left"></i>
                    </a>
                    <a href="#slider-carousel" class="right control-carousel hidden-xs" data-slide="next">
                        <i class="fa fa-angle-right"></i>
                    </a>
                    @endif
                </div>

            </div>
        </div>
    </div>
</section>
<!--/slider-->
@endif