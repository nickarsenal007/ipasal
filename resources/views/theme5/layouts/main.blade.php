<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Home | E-Shopper</title>
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link href="{{ asset('themes/5/css/bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ asset('themes/5/css/font-awesome.min.css') }}" rel="stylesheet">
    <link href="{{ asset('themes/5/css/prettyPhoto.css') }}" rel="stylesheet">
    <link href="{{ asset('themes/5/css/price-range.css') }}" rel="stylesheet">
    <link href="{{ asset('themes/5/css/animate.css') }}" rel="stylesheet">
    <link href="{{ asset('themes/5/css/main.css') }}" rel="stylesheet">
    <link href="{{ asset('themes/5/css/responsive.css') }}" rel="stylesheet">
    <!--[if lt IE 9]>
    <script src="js/html5shiv.js"></script>
    <script src="js/respond.min.js"></script>
    <![endif]-->
    <link rel="shortcut icon" href="{{ asset('themes/5/images/ico/favicon.ico') }}">
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="{{ asset('themes/5/images/ico/apple-touch-icon-144-precomposed.png') }}">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="{{ asset('themes/5/images/ico/apple-touch-icon-114-precomposed.png') }}">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="{{ asset('themes/5/images/ico/apple-touch-icon-72-precomposed.png') }}">
    <link rel="apple-touch-icon-precomposed" href="{{ asset('themes/5/images/ico/apple-touch-icon-57-precomposed.png') }}">
</head>
<!--/head-->

<body>
    <header id="header">
        <!--header-->

        @include('theme5.layouts.head')

        @include('theme5.layouts.menu')
    </header>
    <!--/header-->

    @yield('slider')

    @yield('content')

    @yield('categories')

    <footer id="footer">
        <!--Footer-->
        <div class="footer-top">
            <div class="container">
                <div class="row">
                    <div class="col-sm-2">
                        <div class="companyinfo">
                            <h2><span>e</span>-shopper</h2>
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit,sed do eiusmod tempor</p>
                        </div>
                    </div>
                    <div class="col-sm-2">
                        <div class="single-widget">
                            <h2>Service</h2>
                            <ul class="nav nav-pills nav-stacked">
                                <li><a href="#">Contact Us</a></li>
                                <li><a href="#">FAQ’s</a></li>
                                <li><a href="#">Privacy Policy</a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-sm-3 col-sm-offset-1">
                        <div class="single-widget">
                            <h2>Subscribe Newsletter</h2>
                            <form action="#" class="searchform">
                                <input type="text" placeholder="Your email address" />
                                <button type="submit" class="btn btn-default"><i class="fa fa-arrow-circle-o-right"></i></button>
                                <p>Get the most recent updates from <br />our site and be updated your self...</p>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="footer-widget">
            <div class="container">
                <div class="row">



                </div>
            </div>
        </div>

        <div class="footer-bottom">
            <div class="container">
                <div class="row">
                    <p class="pull-left">Copyright © 2013 E-SHOPPER Inc. All rights reserved.</p>
                    <p class="pull-right">Designed by <span><a target="_blank" href="http://www.themeum.com">Themeum</a></span></p>
                </div>
            </div>
        </div>

    </footer>
    <!--/Footer-->

    <script src="{{ asset('themes/5/js/jquery.js') }}"></script>
    <script src="{{ asset('themes/5/js/bootstrap.min.js') }}"></script>
    <script src="{{ asset('themes/5/js/jquery.scrollUp.min.js') }}"></script>
    <script src="{{ asset('themes/5/js/price-range.js') }}"></script>
    <script src="{{ asset('themes/5/js/jquery.prettyPhoto.js') }}"></script>
    <script src="{{ asset('themes/5/js/main.js') }}"></script>

    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>
    @yield('footer')

    <script>
        $(document).ready(function() {

            // Add to Cart 
            $('.add-to-cart').click(function(e) {
                e.preventDefault();

                let productId = $(this).data('productid');
                let productName = $(this).data('product');
                let rate = $(this).data('rate');

                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });

                $.ajax({
                    type: 'POST',
                    url: '{{ url("cart/ajax") }}',
                    data: {
                        productId,
                        productName,
                        rate
                    },
                    success: function(data) {
                        console.log(data);
                        if (data.status == 200) {

                            let count = $('.total-count').text();
                            let cartcount = parseInt(count) + 1;
                            $('.total-count').text(cartcount);
                            $('.cart-count').text(cartcount + ' Item(s)');

                            Swal.fire({
                                title: 'Success!',
                                text: 'Item added to your cart.',
                                icon: 'success',
                                confirmButtonText: 'Ok'
                            })

                        } else {

                            Swal.fire({
                                text: 'Item already on your cart.',
                                icon: 'error',
                                confirmButtonText: 'Exit'
                            })
                        }
                    }
                });

            });

            // Add to Wish List
            $('.add-to-wishlist').click(function(e) {
                e.preventDefault();

                let productId = $(this).data('product');

                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });

                $.ajax({
                    type: 'POST',
                    url: '{{ url("wishlist/ajax") }}',
                    data: {
                        productId
                    },
                    success: function(data) {

                        if (data.status == 200) {


                            Swal.fire({
                                title: 'Success!',
                                text: data.message,
                                icon: 'success',
                                confirmButtonText: 'Ok'
                            })

                        } else {

                            Swal.fire({
                                text: 'Please login to add this item in the wish list.',
                                icon: 'error',
                                confirmButtonText: 'Exit'
                            })

                        }

                    }
                });

            });

            // Sort Product
            $('.sort-product').change(function() {

                window.location = `?sort=` + $(this).val();

            });

        });
    </script>
</body>

</html>