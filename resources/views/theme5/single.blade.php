@extends('theme5.layouts.main')

@section('content')

<section>
	<div class="container">
		<div class="row">
			<?php $categories = App\Category::where('parentId', 0)->get()->take(4); ?>
            <div class="col-sm-3">
                <div class="left-sidebar">
                    <h2>Category</h2>
                    <div class="panel-group category-products" id="accordian">
                        <!--category-productsr-->
                        @foreach($categories as $category)
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4 class="panel-title">
                                    <a data-toggle="collapse" data-parent="#accordian" href="#{{ $category->slug }}">
                                        @if( $category->children() )
                                        <span class="badge pull-right"><i class="fa fa-plus"></i></span>
                                        @endif
                                        {{ $category->name }}
                                    </a>
                                </h4>
                            </div>
                            @if( $category->children() )
                            <div id="{{ $category->slug }}" class="panel-collapse collapse">
                                <div class="panel-body">
                                    <ul>
                                        @foreach( $category->children()->take(4) as $child)
                                        <li><a href="{{ route('category.product.new5', $child->slug) }}">{{ $child->name }} </a></li>
                                        @endforeach
                                    </ul>
                                </div>
                            </div>
                            @endif
                        </div>
                        @endforeach
                    </div>
                    <!--/category-products-->

                    <div class="shipping text-center">
                        <!--shipping-->
                        <a href="{{ url('theme5') }}"><img src="{{ asset('themes/5/images/home/shipping.jpg') }}" alt="" /></a>
                    </div>
                    <!--/shipping-->

                </div>
            </div>
			
			<div class="col-sm-9 padding-right">
				<div class="product-details"><!--product-details-->
					<div class="col-sm-5">
						<div class="view-product">
							<img src="{{  asset('uploads/products/'.$product->featuredImage) }}" alt="" />
							@if( $product->rate < $product->actualRate )
							<h3>{{ $product->discountPercent }}% Off</h3>
							@endif
						</div>
					</div>
					<div class="col-sm-7">
						<div class="product-information"><!--/product-information-->
							<h2>{{ $product->productName }}</h2>
							<p>{{ $product->reviews()->count() }} Review(s)</p>
							<style>
		                      	.yellow{ color: orange }
							</style>
							@if( $averageRating > 0 )
							<i class="{{ $averageRating >= 1 ? 'yellow' : '' }} fa fa-star"></i>
							<i class="{{ $averageRating >= 2 ? 'yellow' : '' }} fa fa-star"></i>
							<i class="{{ $averageRating >= 3 ? 'yellow' : '' }} fa fa-star"></i>
							<i class="{{ $averageRating >= 4 ? 'yellow' : '' }} fa fa-star"></i>
							<i class="{{ $averageRating >= 5 ? 'yellow' : '' }} fa fa-star"></i>
							@endif
							<span>
								<br>
								<span>NRs. {{ $product->rate }}</span>
								<a class="add-to-cart" href="" data-productid="{{ $product->id }}" data-product="{{ $product->productName }}" data-rate="{{ $product->rate }}">
									<button type="button" class="btn btn-default cart">
									<i class="fa fa-shopping-cart"></i>
										Add to cart
									</button>
								</a>
							</span>
							<p>{{ $product->shortDesc }}</p>
							<p><b>Availability:</b> In Stock</p>
							<p><b>Category:</b> {{ $product->category->name }}</p>
							<a href=""><img src="{{ asset('themes/5/images/product-details/share.png') }}" class="share img-responsive"  alt="" /></a>
						</div><!--/product-information-->
					</div>
				</div><!--/product-details-->
				
				<div class="category-tab shop-details-tab"><!--category-tab-->
					<div class="col-sm-12">
						<ul class="nav nav-tabs">
							<li><a href="#details" data-toggle="tab">Details</a></li>
							<li class="active"><a href="#reviews" data-toggle="tab">Reviews ({{ $product->reviews()->count() }})</a></li>
						</ul>
					</div>
					<div class="tab-content">
						<div class="tab-pane fade" id="details" >
							<div class="col-md-12">
								{!! $product->description !!}
							</div>
						</div>
						
						<div class="tab-pane fade active in" id="reviews" >
						@foreach($product->reviews()->latest()->get()->take(3) as $review)
							<div class="col-sm-12" style="border:1px solid #ccc;padding:15px;margin-bottom: 10px;">
								<ul>
									<li><a href=""><i class="fa fa-user"></i>{{ $review->user->username }}</a></li>
									<li><a href=""><i class="fa fa-calendar-o"></i>{{ date('Y-m-d', strtotime($review->created_at)) }}</a></li>
								</ul>
								<p>{!! $review->reviewDesc !!}</p>
								
							</div>
						@endforeach
						@auth
							<div class="col-md-12">
								<hr>
								<p><b>Write Your Review</b></p>
								<form action="{{ route('product.review') }}" method="post" class="pl-2">
                      				{{csrf_field()}}
									
									<div class="aa-your-rating">
                     					<div class="form-group" id="rating" style="display: inline-flex;">
								            <small>Rate this product&nbsp;&nbsp;</small>
							            </div>
									</div>
									
									<div class="form-group">
										<label for="name">Review Title</label>
                        				<input type="text" class="form-control" name="reviewTitle" id="name" placeholder="Name">
									</div>
								
									<div class="form-group">
										<label for="">Your Review</label>
										<textarea class="form-control" name="reviewDesc" rows="3" id="message"></textarea>
									</div>
									<input type="hidden" name="user_id" value="{{Auth::id()}}">
			                        <input type="hidden" name="product_id" value="{{$product->id}}">
			                        <input type="hidden" name="email" value="{{Auth::user()->email}}">
									<button type="submit" class="btn btn-default pull-right">
										Submit
									</button>
								</form>
							</div>
						@endauth
						</div>
					</div>
				</div><!--/category-tab-->
				
				<div class="recommended_items"><!--recommended_items-->
					<h2 class="title text-center">Related Products</h2>
					
					<div id="recommended-item-carousel" class="carousel slide" data-ride="carousel">
						<div class="carousel-inner">
						<?php $active = 'active'; ?>
						
							<div class="item {{ $active }}">	
							@foreach( $relatedproducts as $prod )
								<div class="col-sm-4">
									<div class="product-image-wrapper">
										<div class="single-products">
											<div class="productinfo text-center">
												<img src="{{  asset('uploads/products/thumbnails/'.$prod->featuredImage) }}" alt="" />
												<h2>NRs. {{ $prod->rate }}</h2>
												<a href="{{ route('view.product.new5', $prod->slug) }}">
													<p>{{ $prod->productName }}</p>
												</a>
												<button type="button" class="btn btn-default add-to-cart" data-productid="{{ $prod->id }}" data-product="{{ $prod->productName }}" data-rate="{{ $prod->rate }}"><i class="fa fa-shopping-cart"></i>Add to cart</button>
											</div>
										</div>
									</div>
								</div>
							@endforeach
							</div>
						</div>
						@if( count($relatedproducts) > 3 )
						<a class="left recommended-item-control" href="#recommended-item-carousel" data-slide="prev">
						<i class="fa fa-angle-left"></i>
						</a>
						<a class="right recommended-item-control" href="#recommended-item-carousel" data-slide="next">
						<i class="fa fa-angle-right"></i>
						</a>
						@endif			
					</div>
				</div><!--/recommended_items-->
				
			</div>
		</div>
	</div>
</section>

@endsection

@section('footer')

<script src="{{ asset('js/jquery.emojiRatings.min.js') }}"></script>
<script defer src="{{ asset('js/jquery.elevateZoom-3.0.8.min.js') }}"></script>
<script type="text/javascript">
	function change_image(x){
		var nn =x.src ;
		document.getElementById("myImg").src = nn;
		document.getElementById("demo").src = nn;
	}
</script>
<script type="text/javascript">
	$(document).ready(function() {
		$("#zoom_10").elevateZoom({
			easing : true,
			zoomWindowWidth:350,
            zoomWindowHeight:350,
            zoomWindowPosition: 1, zoomWindowOffetx: 250
		});
	});
	$("#rating").emojiRating({
				fontSize: 20,
				onUpdate: function(count) {
					$(".review-text").show();
					$("#starCount").html(count + " Star");
					var rating = $('.emoji-rating').val();
					$('#rating-input').val(rating);
				}
			});


			$('#order-submit').click(function(e){
				$(this).prop('disabled', true);
				$('#oform').submit();
			});
</script>

@endsection