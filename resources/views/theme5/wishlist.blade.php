@extends('theme5.layouts.main')

@section('content')

<section id="cart_items">
		<div class="container">
			<div class="breadcrumbs">
				<ol class="breadcrumb">
				  <li><a href="{{ url('theme5') }}">Home</a></li>
				  <li class="active">{{ $title }}</li>
				</ol>
			</div>
			<div class="table-responsive cart_info">
				<table class="table table-condensed">
					<thead>
						<tr class="cart_menu">
							<td class="image">Item</td>
							<td class="description"></td>
							<td class="price">Price</td>
							<!-- <td class="quantity">Quantity</td> -->
							<td class="total">Total</td>
							<td></td>
						</tr>
					</thead>
					<tbody>
					@if( count($products) )
                            @foreach($products as $product)
						<tr>
							<td class="cart_product">
								<a href=""><img width="50%" height="160" src="{{ asset('uploads/products/thumbnails/'.$product->featuredImage) }}" alt=""></a>
							</td>
							<td class="cart_description">
								<h4><a href="{{ route('view.product.new5', $product->slug) }}">{{ $product->productName }}</a></h4>
							</td>
							<td class="cart_price">
								<p>{{ $product->rate }}/-</p>
							</td>
							<!-- <td class="cart_quantity">
								<div class="cart_quantity_button">
									<a class="cart_quantity_up" href=""> + </a>
									<input class="cart_quantity_input" type="text" name="quantity" value="1" autocomplete="off" size="2">
									<a class="cart_quantity_down" href=""> - </a>
								</div>
							</td> -->
							<td class="cart_total">
								<p class="cart_total_price">NRs. {{ $product->rate }}</p>
							</td>
							<td class="cart_delete">
								<a class="btn hvr-hover add-to-wishlist" data-product="{{ $product->id }}" href="" style="color: white"><i class="fa fa-heart"></i></a>
							</td>
						</tr>
						@endforeach
					@else
						<tr>
							<td colspan="5"><h4>&nbsp;0 items.</h4></td>
						</tr>
					@endif
					</tbody>
				</table>
			</div>
		</div>
	</section> <!--/#cart_items-->

@endsection