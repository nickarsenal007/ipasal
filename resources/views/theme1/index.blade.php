@extends('theme1.layouts.main')

@section('head')

<link rel="stylesheet" type="text/css" href="{{ asset('themes/1/plugins/OwlCarousel2-2.2.1/owl.carousel.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('themes/1/plugins/OwlCarousel2-2.2.1/owl.theme.default.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('themes/1/plugins/OwlCarousel2-2.2.1/animate.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('themes/1/styles/main_styles.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('themes/1/styles/responsive.css') }}">

@endsection

@section('sliders')

	@include('theme1.layouts.sliders')

@endsection

@section('products')

	@include('theme1.layouts.productsection')

@endsection

@section('categorysection')

	@include('theme1.layouts.categorysection')

@endsection

@section('features')

	@include('theme1.layouts.features')

@endsection