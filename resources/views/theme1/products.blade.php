@extends('theme1.layouts.main')

@section('head')

<link rel="stylesheet" type="text/css" href="{{ asset('themes/1/plugins/OwlCarousel2-2.2.1/owl.carousel.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('themes/1/plugins/OwlCarousel2-2.2.1/owl.theme.default.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('themes/1/plugins/OwlCarousel2-2.2.1/animate.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('themes/1/styles/category.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('themes/1/styles/category_responsive.css') }}">
<style>
	.product_price{ font-size: 25px }
</style>

@endsection

@section('content')

<div class="home">
	<div class="home_container d-flex flex-column align-items-center justify-content-end">
		<div class="home_content text-center">
			<div class="home_title">{{ $title }}</div>
			<div class="breadcrumbs d-flex flex-column align-items-center justify-content-center">
				<ul class="d-flex flex-row align-items-start justify-content-start text-center">
					<li><a href="#">Home</a></li>
					@if( isset($catname) )
					<li><a href="{{ route('category.product.new', $catname->slug) }}">{{ $catname->name }}</a></li>
					@endif
				</ul>
			</div>
		</div>
	</div>
</div>
<?php $tags = App\Tags::orderBy('name')->get()->take(3); ?>
<div class="products">
			<div class="container">
		<div class="row products_bar_row">
			<div class="col">
				<div class="products_bar d-flex flex-lg-row flex-column align-items-lg-center align-items-start justify-content-lg-start justify-content-center">
					<div class="products_bar_links">
						<ul class="d-flex flex-row align-items-start justify-content-start">
							<li><a href="#" class="active">All</a></li>
						@foreach($tags as $tag)
							<li><a href="">{{ $tag->name }}</a></li>			
						@endforeach
						</ul>
					</div>
					<div class="products_bar_side d-flex flex-row align-items-center justify-content-start ml-lg-auto">
						<div class="products_dropdown product_dropdown_sorting">
							<div class="isotope_sorting_text"><span>Default Sorting</span><i class="fa fa-caret-down" aria-hidden="true"></i></div>
							<ul>
								<li class="item_sorting_btn">Default</li>
								<li class="item_sorting_btn">Low to High</li>
								<li class="item_sorting_btn">High to Low</li>
							</ul>
						</div>
						<div class="products_dropdown text-right product_dropdown_filter">
							<div class="isotope_filter_text"><span>Filter</span><i class="fa fa-caret-down" aria-hidden="true"></i></div>
							<ul>
								<li class="item_filter_btn" data-filter="*">All</li>
							@foreach( $tags as $tag )
								<li class="item_filter_btn" data-filter=".{{$tag->name}}">{{ $tag->name }}</li>
							@endforeach
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="row products_row products_container grid">
			@if( count($products) > 0 )
			<!-- Product -->
			@foreach( $products as $product )
			<div class="col-xl-4 col-md-6 grid-item new">
				<div class="product">
					<div class="product_image"><img src="{{ asset('uploads/products/thumbnails/'. $product->featuredImage) }}" alt=""></div>
					<div class="product_content">
						<div class="product_info d-flex flex-row align-items-start justify-content-start">
							<div>
								<div>
									<div class="product_name"><a href="{{ route('view.product.new', $product->slug) }}">{{ $product->productName }}</a></div>
									<div class="product_category">In <a href="{{ route('category.product.new', str_slug($product->categoryName)) }}">{{ $product->categoryName }}</a></div>
								</div>
							</div>
							<div class="ml-auto text-right">
								<div class="product_price text-right">{{ $product->rate }}</div>
							</div>
						</div>
						<div class="product_buttons">
							<div class="text-right d-flex flex-row align-items-start justify-content-start">
								<div onclick="window.location = '{{ route('wishlist.add', $product->id) }}'" class="product_button product_fav text-center d-flex flex-column align-items-center justify-content-center">
									<div><div><img src="{{ asset('themes/1/images/heart_2.svg') }}" class="svg" alt=""><div>+</div></div></div>
								</div>
								<div onclick="window.location = '{{ route('view.product.new', $product->slug) }}'" class="product_button product_cart text-center d-flex flex-column align-items-center justify-content-center">
									<div><div><img src="{{ asset('themes/1/images/cart.svg') }}" class="svg" alt=""><div>+</div></div></div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			@endforeach
			@else
			<h4>No products at the moment.</h4>
			@endif
		</div>
		<div class="row page_nav_row">
			<div class="col">
				<div class="page_nav">
					<ul class="d-flex flex-row align-items-start justify-content-center">
						{{ $products->links() }}
					</ul>
				</div>
			</div>
		</div>
	</div>
</div>

@endsection