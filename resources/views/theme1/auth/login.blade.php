@extends('theme1.layouts.main')

@section('head')

<link rel="stylesheet" type="text/css" href="{{ asset('themes/1/styles/cart.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('themes/1/styles/cart_responsive.css') }}">
<link rel="stylesheet" href="{{ asset('css/login.css') }}">
<style>
.terms{
	border: 1px solid lightgrey;
    height: 500px;
    overflow-y: scroll;
    padding: 30px;
    background-color: white;
    width: 68%;
    font-size: 13px;
}	
@media (max-width: 560px) {
	.terms{
		height: 350px;
		width: 100%;
		font-size: 12px;
	}
}
</style>

@endsection

@section('content')

<div class="home">
	<div class="home_container d-flex flex-column align-items-center justify-content-end">
		<div class="home_content text-center">
			<div class="home_title">Login / Register</div>
			<div class="breadcrumbs d-flex flex-column align-items-center justify-content-center">&nbsp;</div>
		</div>
	</div>
</div>

@include('theme1.auth.login-form')

@endsection