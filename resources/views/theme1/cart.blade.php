@extends('theme1.layouts.main')

@section('head')

<link rel="stylesheet" type="text/css" href="{{ asset('themes/1/styles/cart.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('themes/1/styles/cart_responsive.css') }}">
<style>
	.remove-item{ 
	    border: none;
    	background: white;
    	font-size: 14px;
    	cursor: pointer;
	    padding: 3px 5px 4px 8px;
 	}
</style>
@endsection

@section('content')

<div class="home">
	<div class="home_container d-flex flex-column align-items-center justify-content-end">
		<div class="home_content text-center">
			<div class="home_title">Shopping Cart</div>
			<div class="breadcrumbs d-flex flex-column align-items-center justify-content-center">
				<ul class="d-flex flex-row align-items-start justify-content-start text-center">
					<li><a href="{{ url('theme1') }}">Home</a></li>
					<li>Your Cart</li>
				</ul>
			</div>
		</div>
	</div>
</div>

<div class="cart_section">
			<div class="container">
				<div class="row">
					<div class="col">
						<div class="cart_container">
							
							<!-- Cart Bar -->
							<div class="cart_bar">
								<ul class="cart_bar_list item_list d-flex flex-row align-items-center justify-content-end">
									<li class="mr-auto">Product</li>
									<li>Price</li>
									<li></li>
									<li>Total</li>
								</ul>
							</div>

							<!-- Cart Items -->
							<div class="cart_items">
								<ul class="cart_items_list">
								<?php $i = 1; ?>
								@foreach (Cart::content() as $item)
									<!-- Cart Item -->
									<li class="cart_item item_list d-flex flex-lg-row flex-column align-items-lg-center align-items-start justify-content-lg-end justify-content-start">
										<div class="product d-flex flex-lg-row flex-column align-items-lg-center align-items-start justify-content-start mr-auto">
											<div><div class="product_number">{{ $i }}.</div></div>
											<div><div class="product_image"><img src="{{asset('uploads/products/thumbnails/'.$item->model->featuredImage)}}" alt=""></div></div>
											<div class="product_name_container">
												<div class="product_name"><a href="{{ route('view.product', $item->model->slug) }}">{{ $item->model->productName }}</a></div>
												<div class="product_text">{{ $item->model->categoryName }}</div>
											</div>
										</div>
										<div class="product_price product_text"><span>Price: </span>{{ $item->model->rate }}</div>
										<div class="product_quantity_container">
											<div class="product_quantity ml-lg-auto mr-lg-auto text-center">
												<form action="{{ route('cart.destroy', $item->rowId) }}" method="post">
													{{csrf_field()}}
													{{ method_field('delete') }}
													<button type="submit" class="remove-item">Remove</button>
												</form>
											</div>
										</div>
										<div class="product_total product_text">{{$item->subtotal}}/-</div>
									</li>
									<?php $i++; ?>
								@endforeach
								</ul>
							</div>

							<!-- Cart Buttons -->
							<div class="cart_buttons d-flex flex-row align-items-start justify-content-start">
								<div class="cart_buttons_inner ml-sm-auto d-flex flex-row align-items-start justify-content-start flex-wrap">
									<div class="button button_continue trans_200"><a href="{{ url('/theme1') }}">continue shopping</a></div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="row cart_extra_row">
					<div class="col-lg-6">
						<div class="cart_extra cart_extra_1">
							<div class="cart_extra_content cart_extra_coupon">
								<div class="cart_extra_title">Coupon code</div>
								<div class="coupon_form_container">
									<form action="#" id="coupon_form" class="coupon_form">
										<input type="text" class="coupon_input" required="required">
										<button class="coupon_button">apply</button>
									</form>
								</div>
								<div class="coupon_text">Phasellus sit amet nunc eros. Sed nec congue tellus. Aenean nulla nisl, volutpat blandit lorem ut.</div>
								<div class="shipping">
									<div class="cart_extra_title">Shipping Method</div>
									<ul>
										<li class="shipping_option d-flex flex-row align-items-center justify-content-start">
											<label class="radio_container">
												<input type="radio" id="radio_2" name="shipping_radio" class="shipping_radio">
												<span class="radio_mark"></span>
												<span class="radio_text">Standard delivery</span>
											</label>
											<div class="shipping_price ml-auto">NRs. 50/-</div>
										</li>
										<li class="shipping_option d-flex flex-row align-items-center justify-content-start">
											<label class="radio_container">
												<input type="radio" id="radio_3" name="shipping_radio" class="shipping_radio" checked>
												<span class="radio_mark"></span>
												<span class="radio_text">Free Delivery</span>
											</label>
											<div class="shipping_price ml-auto">Free</div>
										</li>
									</ul>
								</div>
							</div>
						</div>
					</div>
					<div class="col-lg-6 cart_extra_col">
						<div class="cart_extra cart_extra_2">
							<div class="cart_extra_content cart_extra_total">
								<div class="cart_extra_title">Cart Total</div>
								<ul class="cart_extra_total_list">
									<li class="d-flex flex-row align-items-center justify-content-start">
										<div class="cart_extra_total_title">Subtotal</div>
										<div class="cart_extra_total_value ml-auto">{{ Cart::subtotal() }}</div>
									</li>
									<li class="d-flex flex-row align-items-center justify-content-start">
										<div class="cart_extra_total_title">Shipping</div>
										<div class="cart_extra_total_value ml-auto">Free</div>
									</li>
									<li class="d-flex flex-row align-items-center justify-content-start">
										<div class="cart_extra_total_title">Total</div>
										<div class="cart_extra_total_value ml-auto">NRs. {{ Cart::subtotal() }}/-</div>
									</li>
								</ul>
								<div class="checkout_button trans_200"><a href="{{ route('checkout.view') }}">proceed to checkout</a></div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>

@endsection