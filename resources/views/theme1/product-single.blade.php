@extends('theme1.layouts.main')

@section('head')

<link rel="stylesheet" type="text/css" href="{{ asset('themes/1/plugins/flexslider/flexslider.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('themes/1/styles/product.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('themes/1/styles/product_responsive.css') }}">
<style>
.color-orange{
	color:#ffc83d;
}
.form-textarea{
	height: auto;
	text-align: left;
	padding: 10px;
	padding-left: 20px;
}
.form-input{
	height: 39px;
	text-align: left;
	padding-left: 20px
}
.form-input,
.form-textarea
{
	width: 100%;
	border: solid 1px #4a4a4a;
	border-radius: 20px;
	outline: none;
	font-size: 16px;
	color: #2e2e2e;
	font-weight: 300;
	box-shadow: 0 0 1px 0px #4a4a4a inset, 0 0 1px 0px #4a4a4a;
	-webkit-transition: all 200ms ease;
	-moz-transition: all 200ms ease;
	-ms-transition: all 200ms ease;
	-o-transition: all 200ms ease;
	transition: all 200ms ease;
}
.form-input:focus,
.form-input:hover,
.form-textarea:focus,
.form-textarea:hover
{
	border-color: #2fce98;
	box-shadow: 0 0 1px 0px #2fce98 inset, 0 0 1px 0px #2fce98;
}
.form-input::-webkit-input-placeholder
{
	font-size: 16px !important;
	font-weight: 300 !important;
	color: #2e2e2e !important;
}
.form-input:-moz-placeholder
{
	font-size: 16px !important;
	font-weight: 300 !important;
	color: #2e2e2e !important;
}
.form-input::-moz-placeholder
{
	font-size: 16px !important;
	font-weight: 300 !important;
	color: #2e2e2e !important;
} 
.form-input:-ms-input-placeholder
{ 
	font-size: 16px !important;
	font-weight: 300 !important;
	color: #2e2e2e !important;
}
.form-input::input-placeholder
{
	font-size: 16px !important;
	font-weight: 300 !important;
	color: #2e2e2e !important;
}
.btn-submit{ 
    border-radius: 12px;
    background-color: #2fce98;
    border-color: #2fce98;
    color: white;
    font-weight: 600;
}
.reviews{
	padding-bottom: 30px;
}
</style>

@endsection

@section('content')


<div class="home">
	<div class="home_container d-flex flex-column align-items-center justify-content-end">
		<div class="home_content text-center">
			<div class="home_title">Product Page</div>
			<div class="breadcrumbs d-flex flex-column align-items-center justify-content-center">
				<ul class="d-flex flex-row align-items-start justify-content-start text-center">
					<li><a href="#">Home</a></li>
					<li><a href="{{ route('category.product', str_slug($product->categoryName)) }}">{{ $product->categoryName }}</a></li>
					<li>{{ $product->productName }}</li>
				</ul>
			</div>
		</div>
	</div>
</div>

<!-- Product -->

<div class="product">
	<div class="container">
		<div class="row">
			
			<!-- Product Image -->
			<div class="col-lg-6">
				<div class="product_image_slider_container">
					<div id="slider" class="flexslider">
						<ul class="slides">
							<img src="{{  asset('uploads/products/'.$product->featuredImage) }}" alt="">
						</ul>
					</div>
				</div>
			</div>

			<!-- Product Info -->
			<div class="col-lg-6 product_col">
				<div class="product_info">
					<div class="product_name">{{ $product->productName }}</div>
					<div class="product_category">In <a href="{{ route('category.product', str_slug($product->categoryName)) }}">{{ $product->categoryName }}</a></div>
					<div class="product_rating_container d-flex flex-row align-items-center justify-content-start">
						
							<?php if ($averageRating > 0): ?>
							<p>
	  							<i class="fa fa-star {{ $averageRating >= 1 ? 'color-orange' : '' }}" aria-hidden="true"></i>
	  							<i class="fa fa-star {{ $averageRating >= 2 ? 'color-orange' : '' }}" aria-hidden="true"></i>
	  							<i class="fa fa-star {{ $averageRating >= 3 ? 'color-orange' : '' }}" aria-hidden="true"></i>
	  							<i class="fa fa-star {{ $averageRating >= 4 ? 'color-orange' : '' }}" aria-hidden="true"></i>
	  							<i class="fa fa-star {{ $averageRating >= 5 ? 'color-orange' : '' }}" aria-hidden="true"></i>
	  						</p>
			            	<?php else: ?>
	              			<a href="#ratehere" class="anim" style="color: coral;font-weight: 500;margin-left: 15px;">Rate this product.</a>
			            	<?php endif; ?>
						
						<div class="product_reviews">{{ $product->nosReview }}</div>
						<div class="product_reviews_link"><a href="#ratehere">Reviews</a></div>
					</div>
					<div class="product_price">
						<span class="float-left" style="font-weight:500;font-size:30px">NRs.</span>
							@if($product->actualRate)
							<span class="float-left" style="text-decoration: line-through;font-weight:500;font-size:28px">{{ $product->actualRate.' ' }}</span>
							@endif
						<span class="float-left" style="font-weight:500;font-size:30px">{{$product->rate}}</span>
					</div>
					<br>
					<div class="product_text">
						<p>{{ $product->shortDesc }}</p>
					</div>
					<div class="product_buttons">
						<div class="text-right d-flex flex-row align-items-start justify-content-start">
							<div onclick="window.location = '{{ route('wishlist.add', $product->id) }}'" class="product_button product_fav text-center d-flex flex-column align-items-center justify-content-center">
								<div><div><img src="{{ asset('themes/1/images/heart_2.svg') }}" class="svg" alt=""><div>+</div></div></div>
							</div>
							<div onclick="document.getElementById('add-to-cart').submit()" class="product_button product_cart text-center d-flex flex-column align-items-center justify-content-center">
								<div>
									<div>
										<img src="{{ asset('themes/1/images/cart.svg') }}" class="svg" alt="">
										<div>+</div>
									</div>
									<form id="add-to-cart" action="{{ route('cart.store') }}" method="post" style="display: none">
										{{csrf_field()}}
										<input type="hidden" name="id" value="{{ $product->id }}">
										<input type="hidden" name="name" value="{{ $product->productName }}">
										<input type="hidden" name="price" value="{{ $product->rate }}">
										<button type="submit"><i class="fas fa-shopping-cart"></i>ADD TO CART</button>
									</form>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="boxes">
	<div class="container">
		<div class="row">
			<div class="col-lg-6">
				<div class="box d-flex flex-row align-items-center justify-content-start">
					<div class="mt-auto"><div class="box_image"><img src="{{asset('themes/1/images/boxes_1.png')}}" alt=""></div></div>
					<div class="box_content">
						<div class="box_title">Original</div>
						<div class="box_text">Original Products</div>
					</div>
				</div>
			</div>
			<div class="col-lg-6 box_col">
				<div class="box d-flex flex-row align-items-center justify-content-start">
					<div class="mt-auto"><div class="box_image"><img src="{{ asset('themes/1/images/boxes_2.png') }}" alt=""></div></div>
					<div class="box_content">
						<div class="box_title">Shipping</div>
						<div class="box_text">Shipping cost less than Rs. 80</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="container">
	<div class="row">
		<div class="col-lg-12 col-md-12">
			<div id="ratehere"></div>
		</div>
	</div>
</div>
@auth
<div class="reviews">
	<div class="container">
		<div class="row">
			<div class="col col-lg-6 offset-lg-3 pb-3 pt-1">
				
				<h3>Add a Review:</h3>
				@include('errors.errors')
				<form action="{{ route('product.review') }}" method="post">
					{{csrf_field()}}
					
					<div class="form-group">
						<input type="text" name="reviewTitle" class="form-input" placeholder="Review Title (required)" value="{{ old('reviewTitle') }}">
					</div>

					<div class="form-group" id="rating" style="display: inline-flex;">
						<small>Rate this product&nbsp;&nbsp;</small>
					</div>

					<textarea name="reviewDesc" rows="6" class="form-textarea" placeholder="Review Here...">{{old("reviewDesc")}}</textarea>
					<input type="hidden" name="user_id" value="{{ Auth::id() }}">
					<input type="hidden" name="product_id" value="{{ $product->id }}">
					<input type="hidden" name="email" value="{{ Auth::user()->email }}">
					<div class="form-group">
						<input type="submit" name="submit" value="Submit" class="btn btn-submit btn-block">
					</div>

				</form>
				

			</div>
		</div>
	</div>
</div>
@endauth

@endsection

@section('footer')
<script src="{{ asset('js/jquery.emojiRatings.min.js') }}"></script>
<script defer src="{{ asset('js/jquery.elevateZoom-3.0.8.min.js') }}"></script>
<script type="text/javascript">
	function change_image(x){
		var nn =x.src ;
		document.getElementById("myImg").src = nn;
		document.getElementById("demo").src = nn;
	}
</script>
<script type="text/javascript">
	$(document).ready(function() {
		$("#zoom_10").elevateZoom({
			easing : true,
			zoomWindowWidth:350,
            zoomWindowHeight:350,
            zoomWindowPosition: 1, zoomWindowOffetx: 250
		});
	});
	$("#rating").emojiRating({
				fontSize: 20,
				onUpdate: function(count) {
					$(".review-text").show();
					$("#starCount").html(count + " Star");
					var rating = $('.emoji-rating').val();
					$('#rating-input').val(rating);
				}
			});


			$('#order-submit').click(function(e){
				$(this).prop('disabled', true);
				$('#oform').submit();
			});
</script>
@endsection