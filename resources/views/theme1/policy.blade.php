@extends('theme1.layouts.main')

@section('head')

<link rel="stylesheet" type="text/css" href="{{ asset('themes/1/styles/cart.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('themes/1/styles/cart_responsive.css') }}">

@endsection

@section('content')

<div class="home">
	<div class="home_container d-flex flex-column align-items-center justify-content-end">
		<div class="home_content text-center">
			<div class="home_title">Terms and Privacy Policy</div>
			<div class="breadcrumbs d-flex flex-column align-items-center justify-content-center">
				<ul class="d-flex flex-row align-items-start justify-content-start text-center">
					<li><a href="{{ url('/theme1') }}">Home</a></li>
					<li>Terms and Privacy Policy</li>
				</ul>
			</div>
		</div>
	</div>
</div>

<div class="cart_section">
	<div class="container">
		<div class="row">
			<div class="col">
				<div class="cart_container">
	
					{!! App\Setting::first() ? App\Setting::first()->privacyPolicy : '' !!}

				</div>
			</div>
		</div>
	</div>
</div>

@endsection