<div class="home">
	<!-- Home Slider -->
	<div class="home_slider_container">
		<div class="owl-carousel owl-theme home_slider">
			<?php $sliders = App\Sliders::latest()->get()->take(3);?>
			<!-- Slide -->
			@foreach( $sliders as $slide )
			<div class="owl-item">
				<div class="background_image" style="background-image:url({{ 'uploads/sliders/' . $slide->sliderImage }})"></div>
				<div class="container fill_height">
					<div class="row fill_height">
						<div class="col fill_height">
							<div class="home_container d-flex flex-column align-items-center justify-content-start">
								<div class="home_content">
									<div class="home_title">{{ $slide->textMain }}</div>
									<div class="home_subtitle">{{ $slide->textSecondary }}</div>
									<!-- Get one product of this category -->
									<?php $category = App\Category::find($slide->categoryId); ?>
									@if( $product = $category->products()->latest()->first() )
									<div class="home_items">
										<div class="row">
											<div class="col-sm-3 offset-lg-1">
											@if( count( $product->images ) > 0 )
												<div class="home_item_side"><a href="{{ route('view.product.new', $product->slug) }}"><img src="{{ asset('uploads/products/thumbnails/'. $product->images[0]->image) }}" alt=""></a></div>
											@endif
											</div>
											<div class="col-lg-4 col-md-6 col-sm-8 offset-sm-2 offset-md-0">
												<div class="product home_item_large">
													<div class="product_tag d-flex flex-column align-items-center justify-content-center">
														<div>
															<div>{{ $product->rate }}</div>
														</div>
													</div>
													<div class="product_image"><img src="{{ asset('uploads/products/thumbnails/'. $product->featuredImage) }}" alt=""></div>
													<div class="product_content">
														<div class="product_info d-flex flex-row align-items-start justify-content-start">
															<div>
																<div>
																	<div class="product_name"><a href="{{ route('view.product.new', $product->slug) }}">{{ $product->productName }}</a></div>
																	<div class="product_category">In <a href="">{{ $slide->categoryName }}</a></div>
																</div>
															</div>
															<div class="ml-auto text-right">
																<div class="product_price text-right">{{ $product->discountPercent }}</div>
															</div>
														</div>
														<div class="product_buttons">
															<div class="text-right d-flex flex-row align-items-start justify-content-start">
																<div class="product_button product_fav text-center d-flex flex-column align-items-center justify-content-center">
																	<div><div><img src="{{ asset('themes/1/images/heart.svg') }}" alt=""><div>+</div></div></div>
																</div>
																<div class="product_button product_cart text-center d-flex flex-column align-items-center justify-content-center">
																	<div><div><img src="{{ asset('themes/1/images/cart_2.svg') }}" alt=""><div>+</div></div></div>
																</div>
															</div>
														</div>
													</div>
												</div>
											</div>
											<div class="col-sm-3">
											@if( count( $product->images ) > 1 )
												<div class="home_item_side"><a href="{{ route('view.product.new', $product->slug) }}"><img src="{{ asset('uploads/products/thumbnails/'. $product->images[1]->image) }}" alt=""></a></div>
											@endif
											</div>
										</div>
									</div>
									@endif
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			@endforeach
			
		</div>

		@if( count( $sliders ) > 1 )
		<div class="home_slider_nav home_slider_nav_prev"><i class="fa fa-chevron-left" aria-hidden="true"></i></div>
		<div class="home_slider_nav home_slider_nav_next"><i class="fa fa-chevron-right" aria-hidden="true"></i></div>
		<!-- Home Slider Dots -->
		
		<div class="home_slider_dots_container">
			<div class="container">
				<div class="row">
					<div class="col">
						<div class="home_slider_dots">
							<ul id="home_slider_custom_dots" class="home_slider_custom_dots d-flex flex-row align-items-center justify-content-center">
								<li class="home_slider_custom_dot active">01</li>
								<li class="home_slider_custom_dot">02</li>
								<li class="home_slider_custom_dot">03</li>
								<li class="home_slider_custom_dot">04</li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>

		@endif
	</div>
</div>