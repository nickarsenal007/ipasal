<?php $setting = App\Setting::first() ?>
<!DOCTYPE html>
<html lang="en">
<head>
<title>{{ $setting->siteName ? $setting->siteName : 'Theme: Little Closet' }}</title>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="description" content="Little Closet template">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" type="text/css" href="{{ asset('themes/1/styles/bootstrap-4.1.2/bootstrap.min.css') }}">
<link href="{{ asset('themes/1/plugins/font-awesome-4.7.0/css/font-awesome.min.css') }}" rel="stylesheet" type="text/css">
<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.css">

	@yield('head')

</head>
<body>

<!-- Menu -->

@include('theme1.layouts.menu')

<div class="super_container">

	<!-- Header -->

	<header class="header">
		<div class="header_overlay"></div>
		<div class="header_content d-flex flex-row align-items-center justify-content-start">
			<div class="logo">
				<a href="{{ url('theme1') }}">
					<div class="d-flex flex-row align-items-center justify-content-start">
						<div><img src="{{ $setting->siteLogo ? asset( $setting->siteLogo ) : asset('themes/1/images/logo_1.png') }}" width="60" height="60" alt=""></div>
						<div>{{ $setting->siteName ? $setting->siteName : 'LittleCloset Theme' }}</div>
					</div>
				</a>	
			</div>
			<div class="hamburger"><i class="fa fa-bars" aria-hidden="true"></i></div>
			<div class="header_right d-flex flex-row align-items-center justify-content-start ml-auto">
				<!-- Search -->
				<div class="header_search">
					<form action="#" id="header_search_form">
						<input type="text" class="search_input" placeholder="Search Item" required="required">
						<button class="header_search_button"><img src="images/search.png" alt=""></button>
					</form>
				</div>
				<!-- Cart -->
				<div class="user">
					<a href="{{ route('cart.view') }}">
						<div>
							<img class="svg" src="{{ asset('themes/1/images/cart.svg') }}" alt="https://www.flaticon.com/authors/freepik">
							@if( Cart::instance('default')->count() > 0 )
							<div>{{Cart::instance('default')->count()}}</div>
							@endif
						</div>
					</a>
				</div>
				@auth
					<div class="header_phone d-flex flex-row align-items-center justify-content-start">
						<div><a href="{{ url('theme1/wish-list/' . Auth::id()) }}" style="color: grey">Wish List</a></div>
					</div>
				@endauth
				<!-- Phone -->
				@guest
				<div class="header_phone d-flex flex-row align-items-center justify-content-start">
					<div><a href="{{ route('login.new') }}">Login / Register</a></div>
				</div>
				@else
				<div class="header_phone d-flex flex-row align-items-center justify-content-start">
					<div>
						<a href="{{ route('logout') }}" role="button" class="log-out"
							onclick="event.preventDefault();
							document.getElementById('logout-form').submit();">
							{{ __('Logout') }}
						</a>
						<form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
							{{csrf_field()}}
						</form>
					</div>
				</div>
				@endguest
			</div>
		</div>
	</header>

	<div class="super_container_inner">
		<div class="super_overlay"></div>

		@yield('content')

		<!-- Home -->

		@yield('sliders')

		<!-- Products -->

		@yield('products')

		<!-- Boxes -->

		@yield('categorysection')

		<!-- Features -->

		@yield('features')

		<!-- Footer -->

		<footer class="footer">
			<div class="footer_content">
				<div class="container">
					<div class="row">
						
						<!-- About -->
						<div class="col-lg-4 footer_col">
							<div class="footer_about">
								<div class="footer_logo">
									<a href="#">
										<div class="d-flex flex-row align-items-center justify-content-start">
											<div class="footer_logo_icon">
												@if ( $setting->siteLogo )
													<img src="{{ asset( $setting->siteLogo ) }}" alt="" width="60" height="60">
												@endif
											</div>
											<div>{{ $setting->siteName ? $setting->siteName : '' }}</div>
										</div>
									</a>		
								</div>
								<div class="footer_about_text">
									<p>{{ $setting->aboutUs ? $setting->aboutUs : '' }}</p>
								</div>
							</div>
						</div>

						<!-- Footer Links -->
						<div class="col-lg-4 footer_col">
							<div class="footer_menu">
								<div class="footer_title">Support</div>
								<ul class="footer_list">
									<li>
										<a href="#"><div>Featured Products</div></a>
									</li>
									<li>
										<a href="{{ url('privacy-policy') }}"><div>Terms and Conditions</div></a>
									</li>
									<li>
										<a href="#"><div>Contact</div></a>
									</li>
								</ul>
							</div>
						</div>

						<!-- Footer Contact -->
						@include('theme1.layouts.sociallinks')
					</div>
				</div>
			</div>
			<div class="footer_bar">
				<div class="container">
					<div class="row">
						<div class="col">
							<div class="footer_bar_content d-flex flex-md-row flex-column align-items-center justify-content-start">
								<div class="copyright order-md-1 order-2">{{ $setting->copyrightText ? $setting->copyrightText : '' }}</div>
								
								@include('theme1.layouts.featuredcategories')

							</div>
						</div>
					</div>
				</div>
			</div>
		</footer>
	</div>
		
</div>

<script src="{{ asset('themes/1/js/jquery-3.2.1.min.js') }}"></script>
<script src="{{ asset('themes/1/styles/bootstrap-4.1.2/popper.js') }}"></script>
<script src="{{ asset('themes/1/styles/bootstrap-4.1.2/bootstrap.min.js') }}"></script>
<script src="{{ asset('themes/1/plugins/greensock/TweenMax.min.js') }}"></script>
<script src="{{ asset('themes/1/plugins/greensock/TimelineMax.min.js') }}"></script>
<script src="{{ asset('themes/1/plugins/scrollmagic/ScrollMagic.min.js') }}"></script>
<script src="{{ asset('themes/1/plugins/greensock/animation.gsap.min.js') }}"></script>
<script src="{{ asset('themes/1/plugins/greensock/ScrollToPlugin.min.js') }}"></script>
<script src="{{ asset('themes/1/plugins/OwlCarousel2-2.2.1/owl.carousel.js') }}"></script>
<script src="{{ asset('themes/1/plugins/easing/easing.js') }}"></script>
<script src="{{ asset('themes/1/plugins/progressbar/progressbar.min.js') }}"></script>
<script src="{{ asset('themes/1/plugins/parallax-js-master/parallax.min.js') }}"></script>
<script src="{{ asset('themes/1/js/custom.js') }}"></script>
<script async type="text/javascript" src="{{ asset('js/main.js') }}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>

<script>
	@if (Session::has('success'))
		toastr.success('{{ Session::get("success") }}');
	@endif
	@if (Session::has('info'))
		toastr.info('{{ Session::get("info") }}');
	@endif
</script>

@yield('footer')

</body>
</html>