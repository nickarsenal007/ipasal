<div class="products">
			<div class="container">
				<div class="row">
					<div class="col-lg-6 offset-lg-3">
						<div class="section_title text-center">Our Products</div>
					</div>
				</div>
				<?php $categories = App\Category::latest()->get()->take(3); ?>
				<div class="row page_nav_row">
					<div class="col">
						<div class="page_nav">
							<ul class="d-flex flex-row align-items-start justify-content-center">
							@foreach($categories as $category)
								<li><a href="{{ route('category.product.new', $category->slug) }}">{{ $category->name }}</a></li>
							@endforeach
							</ul>
						</div>
					</div>
				</div>
				<div class="row products_row">
				<?php $products = App\Product::latest()->get()->take(6); ?>	
				@foreach( $products as $product )
					<!-- Product -->
					<div class="col-xl-4 col-md-6">
						<div class="product">
							<div class="product_image"><img src="{{ asset( asset('uploads/products/thumbnails/'. $product->featuredImage) ) }}" alt="" style="width:100%"></div>
							<div class="product_content">
								<div class="product_info d-flex flex-row align-items-start justify-content-start">
									<div>
										<div>
											<div class="product_name"><a href="{{ route('view.product.new', $product->slug) }}">{{ $product->productName }}</a></div>
											<div class="product_category">In <a href="{{ route('category.product.new', $product->category->slug) }}">{{ $product->category->name }}</a></div>
										</div>
									</div>
									<div class="ml-auto text-right">
										<div class="product_price text-right">{{ $product->rate }}</div>
									</div>
								</div>
								<div class="product_buttons">
									<div class="text-right d-flex flex-row align-items-start justify-content-start">
										<div onclick='window.location="{{ route('wishlist.add', $product->id) }}"' class="product_button product_fav text-center d-flex flex-column align-items-center justify-content-center">
											<div><div><img src="{{ asset('themes/1/images/heart_2.svg') }}" class="svg" alt=""><div>+</div></div></div>
										</div>
										<div onclick="window.location = '{{ route('view.product.new', $product->slug) }}'" class="product_button product_cart text-center d-flex flex-column align-items-center justify-content-center">
											<div><div><img src="{{ asset('themes/1/images/cart.svg') }}" class="svg" alt=""><div>+</div></div></div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				@endforeach

				</div>
				<div class="row load_more_row">
					<div class="col">
						<div class="button load_more ml-auto mr-auto"><a href="{{ route('more.products.new') }}">load more</a></div>
					</div>
				</div>
			</div>
		</div>