<div class="menu">

	<!-- Search -->
	<div class="menu_search">
		<form action="#" id="menu_search_form" class="menu_search_form">
			<input type="text" class="search_input" placeholder="Search Item" required="required">
			<button class="menu_search_button"><img src="images/search.png" alt=""></button>
		</form>
	</div>
	<!-- Navigation -->
	<div class="menu_nav">
		<?php $categories = App\Category::where('parentId', 0)->get(); ?>
		<ul>
			@foreach( $categories as $category )
				<li><a href="{{ route('category.product.new', $category->slug) }}">{{ $category->name }}</a></li>
			@endforeach
		</ul>
	</div>
</div>