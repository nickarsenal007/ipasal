<div class="col-lg-4 footer_col">
	<div class="footer_contact">
		<div class="footer_title">Stay in Touch</div>
		<div class="newsletter">
			<form action="#" id="newsletter_form" class="newsletter_form">
				<input type="email" class="newsletter_input" placeholder="Subscribe to our Newsletter" required="required">
				<button class="newsletter_button">+</button>
			</form>
		</div>
		<div class="footer_social">
			<div class="footer_title">Social</div>
			<ul class="footer_social_list d-flex flex-row align-items-start justify-content-start">
				<li><a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
				<li><a href="#"><i class="fa fa-youtube-play" aria-hidden="true"></i></a></li>
				<li><a href="#"><i class="fa fa-google-plus" aria-hidden="true"></i></a></li>
				<li><a href="#"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
			</ul>
		</div>
	</div>
</div>