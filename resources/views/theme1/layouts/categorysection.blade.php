<?php $categories = App\Category::latest()->where('featured', 1)->get()->take(2); ?>
@if( count($categories) )
<div class="boxes">
	<div class="container">
		<div class="row">
			<div class="col">
				<div class="boxes_container d-flex flex-row align-items-start justify-content-between flex-wrap">
					@foreach($categories as $category)
					<!-- Box -->
					<div class="box">
						<div class="background_image" style="background-image:url({{ asset('themes/1/images/box_3.jpg') }})"></div>
						<div class="box_content d-flex flex-row align-items-center justify-content-start">
							<div class="box_left">
								<div class="box_image">
									<a href="{{ route('category.product.new', $category->slug) }}">
										<div class="background_image" style="background-image:url({{ asset( $category->image )  }})"></div>
									</a>
								</div>
							</div>
							<div class="box_right text-center">
								<div class="box_title">&nbsp;&nbsp;{{ $category->name }}</div>
							</div>
						</div>
					</div>
					@endforeach

					<!-- Box -->
					<div class="box">
						<div class="background_image" style="background-image:url({{ asset('themes/1/images/box_3.jpg') }})"></div>
						<div class="box_content d-flex flex-row align-items-center justify-content-start">
							<div class="box_left">
								<div class="box_image">
									<a href="category.html">
										<div class="background_image" style="background-image:url({{ asset('themes/1/images/box_3_img.jpg') }})"></div>
									</a>
								</div>
							</div>
							<div class="box_right text-center">
								<div class="box_title">Popular Choice</div>
							</div>
						</div>
					</div>

				</div>
			</div>
		</div>
	</div>
</div>

@endif