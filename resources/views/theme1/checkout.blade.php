@extends('theme1.layouts.main')

@section('head')

<link rel="stylesheet" type="text/css" href="{{ asset('themes/1/styles/checkout.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('themes/1/styles/checkout_responsive.css') }}">

@endsection

@section('content')

<div class="home">
	<div class="home_container d-flex flex-column align-items-center justify-content-end">
		<div class="home_content text-center">
			<div class="home_title">Checkout</div>
			<div class="breadcrumbs d-flex flex-column align-items-center justify-content-center">
				<ul class="d-flex flex-row align-items-start justify-content-start text-center">
					<li><a href="#">Home</a></li>
					<li>Checkout</li>
				</ul>
			</div>
		</div>
	</div>
</div>

<!-- Checkout -->

<div class="checkout">
	<div class="container">
		<div class="row">
			
			<!-- Billing -->
			<div class="col-lg-6">
				<div class="billing">
					<div class="checkout_title">Billing</div>
					<div class="checkout_form_container">
						<form action="#" id="checkout_form" class="checkout_form">
							<div class="row">
								<div class="col-lg-6">
									<!-- Name -->
									<input type="text" id="checkout_name" class="checkout_input" placeholder="First Name" required="required">
								</div>
								<div class="col-lg-6">
									<!-- Last Name -->
									<input type="text" id="checkout_last_name" class="checkout_input" placeholder="Last Name" required="required">
								</div>
							</div>
							<div>
								<!-- Phone no -->
								<input type="phone" id="checkout_phone" class="checkout_input" placeholder="Phone No." required="required">
							</div>
							<div>
								<!-- Email -->
								<input type="phone" id="checkout_email" class="checkout_input" placeholder="Email" required="required">
							</div>
							<div>
								<!-- Address -->
								<input type="text" id="checkout_address" class="checkout_input" placeholder="Address Line 1" required="required">
								<input type="text" id="checkout_address_2" class="checkout_input checkout_address_2" placeholder="Address Line 2" required="required">
							</div>
							<div>
								<!-- City / Town -->
								<input type="text" name="city" class="checkout_input" placeholder="City / Town">
							</div>
							<div>
								<!-- Province -->
								<input type="text" name="province" class="checkout_input" placeholder="Province">
							</div>
							
						</form>
					</div>
				</div>
			</div>

			<!-- Cart Total -->
			<div class="col-lg-6 cart_col">
				<div class="cart_total">
					<div class="cart_extra_content cart_extra_total">
						<div class="checkout_title">Cart Total</div>
						<ul class="cart_extra_total_list">
							<li class="d-flex flex-row align-items-center justify-content-start">
								<div class="cart_extra_total_title">Subtotal</div>
								<div class="cart_extra_total_value ml-auto">{{ Cart::subtotal() }}</div>
							</li>
							<li class="d-flex flex-row align-items-center justify-content-start">
								<div class="cart_extra_total_title">Shipping</div>
								<div class="cart_extra_total_value ml-auto">Free</div>
							</li>
							<li class="d-flex flex-row align-items-center justify-content-start">
								<div class="cart_extra_total_title">Total</div>
								<div class="cart_extra_total_value ml-auto">NRs. <b>{{ Cart::subtotal() }}/-</div>
							</li>
						</ul>
						<div class="payment_options">
							<div class="checkout_title">Payment</div>
							<ul>
								<li class="shipping_option d-flex flex-row align-items-center justify-content-start">
									<label class="radio_container">
										<input type="radio" id="radio_1" name="payment_radio" class="payment_radio">
										<span class="radio_mark"></span>
										<span class="radio_text">Paypal</span>
									</label>
								</li>
								<li class="shipping_option d-flex flex-row align-items-center justify-content-start">
									<label class="radio_container">
										<input type="radio" id="radio_2" name="payment_radio" class="payment_radio">
										<span class="radio_mark"></span>
										<span class="radio_text">Cash on Delivery</span>
									</label>
								</li>
								<li class="shipping_option d-flex flex-row align-items-center justify-content-start">
									<label class="radio_container">
										<input type="radio" id="radio_3" name="payment_radio" class="payment_radio" checked>
										<span class="radio_mark"></span>
										<span class="radio_text">Credit Card</span>
									</label>
								</li>
							</ul>
						</div>
						<div class="cart_text">
							<p>Register to Place Order</p>
						</div>
						<div class="checkout_button trans_200"><a href="checkout.html">place order</a></div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

@endsection