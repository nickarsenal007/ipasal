@extends('theme3.layouts.main')

@section('content')

<!-- catg header banner section -->
  <section id="aa-catg-head-banner">
   <img src="https://picsum.photos/1920/300?grayscale" alt="fashion img">
   <div class="aa-catg-head-banner-area">
     <div class="container">
      <div class="aa-catg-head-banner-content">
        <h2>{{ $product->productName }}</h2>
        <ol class="breadcrumb">
          <li><a href="{{ url('theme3') }}">Home</a></li>         
          <li><a href="{{ route('category.product.new3', $category->slug) }}">{{ $category->name }}</a></li>
        </ol>
      </div>
     </div>
   </div>
  </section>
  <!-- / catg header banner section -->

  <!-- product category -->
  <section id="aa-product-details">
    <div class="container">
      <div class="row">
        <div class="col-md-12">
          <div class="aa-product-details-area">
            <div class="aa-product-details-content">
              <div class="row">
                <!-- Modal view slider -->
                <div class="col-md-5 col-sm-5 col-xs-12">                              
                  <div class="aa-product-view-slider">                                
                    <div id="demo-1" class="simpleLens-gallery-container">
                      <div class="simpleLens-container">
                        <div class="simpleLens-big-image-container"><a data-lens-image="{{  asset('uploads/products/'.$product->featuredImage) }}" class="simpleLens-lens-image"><img src="{{  asset('uploads/products/thumbnails/'.$product->featuredImage) }}" class="simpleLens-big-image"></a></div>
                      </div>
                    </div>
                  </div>
                </div>
                <!-- Modal view content -->
                <div class="col-md-7 col-sm-7 col-xs-12">
                  <div class="aa-product-view-content">
                    <h3>{{ $product->productName }}</h3>
                    <div class="aa-price-block">
                      <style>
                      	.yellow{ color: orange }
                      </style>
                      @if( $averageRating > 0 )
                        <i class="{{ $averageRating >= 1 ? 'yellow' : '' }} fa fa-star"></i>
                        <i class="{{ $averageRating >= 2 ? 'yellow' : '' }} fa fa-star"></i>
                        <i class="{{ $averageRating >= 3 ? 'yellow' : '' }} fa fa-star"></i>
                        <i class="{{ $averageRating >= 4 ? 'yellow' : '' }} fa fa-star"></i>
                        <i class="{{ $averageRating >= 5 ? 'yellow' : '' }} fa fa-star"></i>
                      @endif
                      <p class="aa-product-avilability">Avilability: <span>In stock</span></p>
                    
						<h3>NRs. {{ $product->rate }}</h3>

                    </div>
                    <p>{{ $product->shortDesc }}</p>
                    
                    
                    <div class="aa-prod-quantity">
                      <p class="aa-prod-category">
                        Category: <a href="{{ route('category.product.new3', $category->slug) }}">{{ $category->name }}</a>
                      </p>
                    </div>
                    <div class="aa-prod-view-bottom">
                      <a class="aa-add-to-cart-btn add-to-cart" data-productid="{{ $product->id }}" data-product="{{ $product->productName }}" data-rate="{{ $product->rate }}" href="#">Add To Cart</a>
                      <a class="aa-add-to-cart-btn add-to-wishlist" data-product="{{ $product->id }}" href="#">Wishlist</a>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="aa-product-details-bottom">
              <ul class="nav nav-tabs" id="myTab2">
                <li><a href="#description" data-toggle="tab">Description</a></li>
                <li><a href="#review" data-toggle="tab">Reviews</a></li>                
              </ul>

              <!-- Tab panes -->
              <div class="tab-content">
                <div class="tab-pane fade in active" id="description">
                  {!! $product->description !!}
                </div>
                <div class="tab-pane fade " id="review">
                 <div class="aa-product-review-area">
                   <h4>{{ $product->nosReview }} Reviews for {{ $product->productName }}</h4> 
                   <ul class="aa-review-nav">
                   @foreach($productReviews as $review)
                     <li>
                        <div class="media">
                          <div class="media-body">
                            <h4 class="media-heading"><strong>{{ $review->reviewTitle }}</strong> - <span>{{ $review->created_at }}</span></h4>
                            <!-- <div class="aa-product-rating">
                              <span class="fa fa-star"></span>
                              <span class="fa fa-star"></span>
                              <span class="fa fa-star"></span>
                              <span class="fa fa-star"></span>
                              <span class="fa fa-star-o"></span>
                            </div> -->
                            <p>{{ $review->reviewDesc }}</p>
                          </div>
                        </div>
                      </li>
                    @endforeach
                   </ul>
                   @auth
                   <h4>Add a review</h4>
                   <form action="{{ route('product.review') }}" method="post" class="pl-2">
                      {{csrf_field()}}
                    <div class="aa-your-rating">
                     <div class="form-group" id="rating" style="display: inline-flex;">
						          <small>Rate this product&nbsp;&nbsp;</small>
					           </div>
                    </div>
                   <!-- review form -->
                   <div class="aa-review-form">
                      <div class="form-group">
                        <label for="message">Your Review</label>
                        <textarea class="form-control" name="reviewDesc" rows="3" id="message"></textarea>
                      </div>
                      <div class="form-group">
                        <label for="name">Review Title</label>
                        <input type="text" class="form-control" name="reviewTitle" id="name" placeholder="Name">
                      </div>  
                      <input type="hidden" name="user_id" value="{{Auth::id()}}">
                      <input type="hidden" name="product_id" value="{{$product->id}}">
                      <input type="hidden" name="email" value="{{Auth::user()->email}}">
                      <button type="submit" class="btn btn-default aa-review-submit">Submit</button>
                  	</div>
                   </form>
                   @endauth
                 </div>
                </div>            
              </div>
            </div>
            @if( count( $relatedproducts ) )
            <!-- Related product -->
            <div class="aa-product-related-item">
              <h3>Related Products</h3>
              <ul class="aa-product-catg aa-related-item-slider">
              @foreach( $relatedproducts as $product )
                
                <!-- start single product item -->
                <li>
                  <figure>
                    <a class="aa-product-img" href="{{ route('view.product.new3', $product->slug) }}"><img src="{{  asset('uploads/products/thumbnails/'.$product->featuredImage) }}" alt="no image" width="100%" height="300"></a>
                    <a class="aa-add-card-btn add-to-cart" data-productid="{{ $product->id }}" data-product="{{ $product->productName }}" data-rate="{{ $product->rate }}" href="#"><span class="fa fa-shopping-cart"></span>Add To Cart</a>
                  </figure>
                  <figcaption>
                    <h4 class="aa-product-title"><a href="{{ route('view.product.new3', $product->slug) }}">{{ $product->productName }}</a></h4>
                    <span class="aa-product-price">NRs. {{ $product->rate }}</span>
                  </figcaption>
                  <div class="aa-product-hvr-content">
                    <a href="#" class="btn min add-to-wishlist" data-product="{{ $product->id }}" data-toggle="tooltip" data-placement="top" title="Add to Wishlist"><span class="fa fa-heart-o"></span></a>
                  </div>
                </li>

                @endforeach
                                                                                               
              </ul>
                
            </div> 
            @endif 
          </div>
        </div>
      </div>
    </div>
  </section>
  <!-- / product category -->

  @include('theme3.layouts.newsletter')

@endsection

@section('footer')

<script src="{{ asset('js/jquery.emojiRatings.min.js') }}"></script>
<script defer src="{{ asset('js/jquery.elevateZoom-3.0.8.min.js') }}"></script>
<script type="text/javascript">
	function change_image(x){
		var nn =x.src ;
		document.getElementById("myImg").src = nn;
		document.getElementById("demo").src = nn;
	}
</script>
<script type="text/javascript">
	$(document).ready(function() {
		$("#zoom_10").elevateZoom({
			easing : true,
			zoomWindowWidth:350,
            zoomWindowHeight:350,
            zoomWindowPosition: 1, zoomWindowOffetx: 250
		});
	});
	$("#rating").emojiRating({
				fontSize: 20,
				onUpdate: function(count) {
					$(".review-text").show();
					$("#starCount").html(count + " Star");
					var rating = $('.emoji-rating').val();
					$('#rating-input').val(rating);
				}
			});


			$('#order-submit').click(function(e){
				$(this).prop('disabled', true);
				$('#oform').submit();
			});
</script>

@endsection