@extends('theme3.layouts.main')

@section('content')

<section id="aa-catg-head-banner">
   <img src="https://picsum.photos/1920/300?grayscale" alt="fashion img">
   <div class="aa-catg-head-banner-area">
     <div class="container">
      <div class="aa-catg-head-banner-content">
        <h2>Privacy Policy</h2>
        <ol class="breadcrumb">
          <li><a href="{{ url('theme3') }}">Home</a></li>         
          <li class="active">Privacy Policy</li>
        </ol>
      </div>
     </div>
   </div>
  </section>
  <!-- / catg header banner section -->
<!-- start contact section -->
 <section id="aa-contact">
   <div class="container">
     <div class="row">
       <div class="col-md-12">
         <div class="aa-contact-area">
           <div class="aa-contact-top" style="text-align: left">
             {!! App\Setting::first() ? App\Setting::first()->privacyPolicy : '' !!}
         </div>
     </div>
 </div>
</div>
</div>
</section>

@endsection