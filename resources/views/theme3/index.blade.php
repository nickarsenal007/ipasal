@extends('theme3.layouts.main')

@section('slider')

	@include('theme3.layouts.slider')

@endsection

@section('content')



@endsection

@section('promotional')

	@include('theme3.layouts.promo')

@endsection

@section('productssection')

	@include('theme3.layouts.productssection')

@endsection

@section('banner')

<!-- banner section -->
  <section id="aa-banner">
    <div class="container">
      <div class="row">
        <div class="col-md-12">        
          <div class="row">
            <div class="aa-banner-area">
            <a href="#"><img src="{{ asset('themes/3/img/fashion-banner.jpg') }}" alt="fashion banner img"></a>
          </div>
          </div>
        </div>
      </div>
    </div>
  </section>

@endsection

@section('popular')

	@include('theme3.layouts.popular')

@endsection

@section('services')

	@include('theme3.layouts.services')

@endsection

@section('newsletter')

	@include('theme3.layouts.newsletter')

@endsection