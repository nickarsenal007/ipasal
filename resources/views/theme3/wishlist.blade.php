@extends('theme3.layouts.main')

@section('content')


<!-- catg header banner section -->
  <section id="aa-catg-head-banner">
   <img src="https://picsum.photos/1920/300?grayscale" alt="fashion img">
   <div class="aa-catg-head-banner-area">
     <div class="container">
      <div class="aa-catg-head-banner-content">
        <h2>Wishlist Page</h2>
        <ol class="breadcrumb">
          <li><a href="{{ url('theme3') }}">Home</a></li>                   
          <li class="active">{{ $title }}</li>
        </ol>
      </div>
     </div>
   </div>
  </section>
  <!-- / catg header banner section -->

 <!-- Cart view section -->
 <section id="cart-view">
   <div class="container">
     <div class="row">
       <div class="col-md-12">
         <div class="cart-view-area">
           <div class="cart-view-table aa-wishlist-table">
             <form action="">
               <div class="table-responsive">
                  <table class="table">
                    <thead>
                      <tr>
                        <th></th>
                        <th>Product</th>
                        <th>Price</th>
                        <th>Stock Status</th>
                        <th></th>
                      </tr>
                    </thead>
                    <tbody>
					@if( count($products) > 0 )

                    @foreach ($products as $product)
                      <tr>
                        <td><a href="{{ route('view.product.new3', $product->slug) }}"><img src="{{ asset('uploads/products/thumbnails/' . $product->featuredImage) }}" alt="img"></a></td>
                        <td><a class="aa-cart-title" href="{{ route('view.product.new3', $product->slug) }}">{{ $product->productName }}</a></td>
                        <td>NRs. {{ $product->rate }}</td>
                        <td>In Stock</td>
                        <td><a href="#" class="aa-add-to-cart-btn add-to-cart" title="Add to cart" href="" data-productid="{{$product->id}}" data-product="{{ $product->productName }}" data-rate="{{ $product->rate }}">Add To Cart</a></td>
                      </tr>
                    @endforeach

					@else
						<tr>
							<td colspan="6">No products at the moment.</td>
						</tr>
                    @endif
                      </tbody>
                  </table>
                </div>
             </form>             
           </div>
         </div>
       </div>
     </div>
   </div>
 </section>
 <!-- / Cart view section -->

@endsection