@extends('theme3.layouts.main')

@section('content')


<!-- catg header banner section -->
  <section id="aa-catg-head-banner">
   <img src="https://picsum.photos/1920/300?grayscale" alt="img">
   <div class="aa-catg-head-banner-area">
     <div class="container">
      <div class="aa-catg-head-banner-content">
        <h2>Cart Page</h2>
        <ol class="breadcrumb">
          <li><a href="{{ url('theme3') }}">Home</a></li>                   
          <li class="active">Cart</li>
        </ol>
      </div>
     </div>
   </div>
  </section>
  <!-- / catg header banner section -->

 <!-- Cart view section -->
 <section id="cart-view">
   <div class="container">
     <div class="row">
       <div class="col-md-12">
         <div class="cart-view-area">
           <div class="cart-view-table">
             
               <div class="table-responsive">
                  <table class="table">
                    <thead>
                      <tr>
                        <th></th>
                        <th></th>
                        <th>Product</th>
                        <th>Total</th>
                      </tr>
                    </thead>
                    <tbody>
                    @if( count( Cart::content() ) )
					           @foreach (Cart::content() as $item)
                      <tr>
                        <td><a class="remove" href="" onclick="event.preventDefault();document.getElementById('remove-item-{{$item->model->slug}}').submit();"><fa class="fa fa-close"></fa></a></td>
                        
                        <td><a href="{{ route('view.product.new3', $item->model->slug) }}"><img src="{{asset('uploads/products/thumbnails/'.$item->model->featuredImage)}}" alt="img"></a></td>
                        <td><a class="aa-cart-title" href="{{ route('view.product.new3', $item->model->slug) }}">{{ $item->model->productName }}</a></td>
                        <td>NRs. {{ $item->model->rate }}</td>
                        <form id="remove-item-{{$item->model->slug}}" action="{{ route('cart.destroy', $item->rowId) }}" method="post">
            							{{csrf_field()}}
            							{{ method_field('delete') }}
            						</form>
                      </tr>
                    @endforeach
					@else
                      <tr>
                      	<td>No items on your cart.</td>
                      </tr>
                    @endif                      
                      <tr>
                        <td colspan="6" class="aa-cart-view-bottom">
                          <div class="aa-cart-coupon">
                            <input class="aa-coupon-code" type="text" placeholder="Coupon">
                            <input class="aa-cart-view-btn" type="submit" value="Apply Coupon">
                          </div>
                          <a class="aa-cart-view-btn" href="{{ url('theme3') }}">Continue Shopping</a>
                        </td>
                      </tr>
                      
                      </tbody>
                  </table>
                </div>
             
             <!-- Cart Total view -->
             <div class="cart-view-total">
               <h4>Cart Totals</h4>
               <table class="aa-totals-table">
                 <tbody>
                   <tr>
                     <th>Subtotal</th>
                     <td>NRs. {{Cart::subtotal()}}/-</td>
                   </tr>
                   <tr>
                   	 <th>Shipping</th>
                   	 <td>Free</td>
                   </tr>
                   <tr>
                     <th>Total</th>
                     <td>NRs. {{Cart::subtotal()}}/-</td>
                   </tr>
                 </tbody>
               </table>
               <a href="{{ route('checkout.view3') }}" class="aa-cart-view-btn">Proced to Checkout</a>
             </div>
           </div>
         </div>
       </div>
     </div>
   </div>
 </section>
 <!-- / Cart view section -->

@endsection