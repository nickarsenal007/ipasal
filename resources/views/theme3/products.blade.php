@extends('theme3.layouts.main')

@section('content')

<!-- catg header banner section -->
<section id="aa-catg-head-banner">
	<img src="https://picsum.photos/1920/300?grayscale" alt="fashion img">
	<div class="aa-catg-head-banner-area">
	 <div class="container">
	  <div class="aa-catg-head-banner-content">
	    <h2>{{ $title }}</h2>
	    <ol class="breadcrumb">
	      <li><a href="{{ url('theme3') }}">Home</a></li>         
	      <li class="active">{{ $catname->name }}</li>
	    </ol>
	  </div>
	 </div>
	</div>
</section>
<!-- / catg header banner section -->	

<!-- product category -->
  <section id="aa-product-category">
    <div class="container">
      <div class="row">
        <div class="col-lg-9 col-md-9 col-sm-8 col-md-push-3">
          <div class="aa-product-catg-content">
            <div class="aa-product-catg-head">
              <div class="aa-product-catg-head-left">
                <form action="" class="aa-sort-form">
                  <label for="">Sort by</label>
                  <select class="sort-product" name="sort">
                    <option value="name">Name</option>
					          <option value="low_high" {{ request()->sort == 'low_high' ? 'selected' : '' }}>Price: Low to High</option>
					          <option value="high_low" {{ request()->sort == 'high_low' ? 'selected' : '' }}>Price: High to Low</option>
                  </select>
                </form>
              </div>
              <div class="aa-product-catg-head-right">
                <a id="grid-catg" href="#"><span class="fa fa-th"></span></a>
                <a id="list-catg" href="#"><span class="fa fa-list"></span></a>
              </div>
            </div>
            <div class="aa-product-catg-body">
              <ul class="aa-product-catg">
			@if( count($products) )
                <!-- start single product item -->
            @foreach ( $products as $product )
                <li>
                  <figure>
                    <a class="aa-product-img" href="{{ route('view.product.new3', $product->slug) }}"><img src="{{ asset('uploads/products/thumbnails/' . $product->featuredImage) }}" alt="no img" width="100%" height="300"></a>
                    <a class="aa-add-card-btn add-to-cart" title="Add to cart" href="" data-productid="{{$product->id}}" data-product="{{ $product->productName }}" data-rate="{{ $product->rate }}" href="#"><span class="fa fa-shopping-cart"></span>Add To Cart</a>
                    <figcaption>
                      <h4 class="aa-product-title"><a href="{{ route('view.product.new3', $product->slug) }}">{{ $product->productName }}</a></h4>
                      <span class="aa-product-price">NRs. {{ $product->rate }}</span>
                      @if( $product->rate < $product->actualRate )
                      <span class="aa-product-price"><del>{{ $product->actualRate }}</del></span>
                      @endif
                      <p class="aa-product-descrip">{{ $product->shortDesc }}</p>
                    </figcaption>
                  </figure>                          
                  <div class="aa-product-hvr-content">
                    <a href="#" class="add-to-wishlist" data-product="{{ $product->id }}" data-toggle="tooltip" data-placement="top" title="Add to Wishlist"><span class="fa fa-heart-o"></span></a>
                  </div>
                  <!-- product badge -->
                  @if( $product->discountPercent )
                  	<span class="aa-badge aa-hot" href="#">{{ $product->discountPercent }}% Off</span>
				          @endif
                </li>
            @endforeach
            @else
            	<li>There are no products available.</li>
            @endif                                       
              </ul>
                
            </div>
            <div class="aa-product-catg-pagination">
              <nav>
                {{ $products->links() }}
              </nav>
            </div>
          </div>
        </div>
        <div class="col-lg-3 col-md-3 col-sm-4 col-md-pull-9">
          <aside class="aa-sidebar">
            <!-- single sidebar -->
            <div class="aa-sidebar-widget">
              <h3>Category</h3>
              <ul class="aa-catg-nav">
              	@foreach( App\Category::orderBy('name')->get() as $category )
                <li><a href="{{ route('category.product.new2', $category->slug) }}">{{ $category->name }}</a></li>
                @endforeach
              </ul>
            </div>
            <!-- single sidebar -->
            <div class="aa-sidebar-widget">
              <h3>Tags</h3>
              <div class="tag-cloud">
              	@foreach( App\Tags::orderBy('name')->get() as $tag )
                	<a href="#">{{ $tag->name }}</a>
               	@endforeach
              </div>
            </div>
          </aside>
        </div>
       
      </div>
    </div>
  </section>
  <!-- / product category -->

@endsection

@section('newsletter')

	@include('theme3.layouts.newsletter')

@endsection