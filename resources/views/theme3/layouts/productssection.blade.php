<?php $categories = App\Product::recentProductsCategories(4); ?>
@if( count($categories) > 0 )
<!-- Products section -->
  <section id="aa-product">
    <div class="container">
      <div class="row">
        <div class="col-md-12">
          <div class="row">
            <div class="aa-product-area">
              <div class="aa-product-inner">
                <!-- start prduct navigation -->
                 <ul class="nav nav-tabs aa-products-tab">
                  <?php $active = ''; ?>
                  @foreach($categories as $category)
                    <li class="{{ $active }}"><a href="#{{ $category->slug }}" data-toggle="tab">{{ $category->categoryName }}</a></li>
                  @endforeach
                  </ul>
                  <!-- Tab panes -->
                  <div class="tab-content">
                    <!-- Start men product category -->
                    <?php $active = 'active'; $in = 'in'; ?>
                    @foreach( $categories as $category )
                    <div class="tab-pane fade {{ $in }} {{ $active }}" id="{{ $category->slug }}">
                      <ul class="aa-product-catg">
                        <!-- start single product item -->
                      <?php $products = App\Product::where('categoryId', $category->categoryId)->inRandomOrder()->get()->take(8); ?>
                      
                        <!-- start single product item -->
                      @foreach($products as $product)
                        <li>
                          <figure>
                            <a class="aa-product-img" href="{{ route('view.product.new3', $product->slug) }}"><img src="{{ asset('uploads/products/thumbnails/' . $product->featuredImage) }}" alt="polo shirt img"></a>
                            <a class="aa-add-card-btn add-to-cart" title="Add to cart" href="" data-productid="{{$product->id}}" data-product="{{ $product->productName }}" data-rate="{{ $product->rate }}" href="#"><span class="fa fa-shopping-cart"></span>Add To Cart</a>
                            <figcaption>
                              <h4 class="aa-product-title"><a href="{{ route('view.product.new3', $product->slug) }}">{{ $product->productName }}</a></h4>
                              <span class="aa-product-price">{{ $product->rate }}</span>
                              @if( $product->actualRate > $product->rate )
                              <span class="aa-product-price"><del>{{ $product->actualRate }}</del></span>
                              @endif
                            </figcaption>
                          </figure>                          
                          <div class="aa-product-hvr-content">
                            <a href="#" class="add-to-wishlist" data-product="{{ $product->id }}" data-toggle="tooltip" data-placement="top" title="Add to Wishlist"><span class="fa fa-heart-o"></span></a>
                          </div>
                          <!-- product badge -->
                          @if( $product->discountPercent )
                          <span class="aa-badge aa-hot" href="#">{{ $product->discountPercent }}% Off</span>
                          @endif
                        </li>
                      @endforeach                      
                      </ul>
                      <a class="aa-browse-btn" href="{{ route('category.product.new3', $category->slug) }}">Browse all Product <span class="fa fa-long-arrow-right"></span></a>
                    </div>
                    <?php $active = $in = ''; ?>
                    @endforeach
                    <!-- / men product category -->
                  </div>
                  <!-- quick view modal -->                  
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
  <!-- / Products section -->

  @endif