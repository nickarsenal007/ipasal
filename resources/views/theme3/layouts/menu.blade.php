<!-- menu -->
  <section id="menu">
    <div class="container">
      <div class="menu-area">
        <!-- Navbar -->
        <div class="navbar navbar-default" role="navigation">
          <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
              <span class="sr-only">Toggle navigation</span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
            </button>          
          </div>
          <div class="navbar-collapse collapse">
            <!-- Left nav -->
            <ul class="nav navbar-nav">
              <li><a href="{{ url('theme3') }}">Home</a></li>
              <li><a href="#">Categories <span class="caret"></span></a>
                <ul class="dropdown-menu">                
                @foreach( App\Category::orderBy('name')->where('parentId', 0)->get() as $category )
                  <li>
                    <a href="{{ route('category.product.new3', $category->slug) }}">{{ $category->name }}
                      @if( $category->children() )
                        <span class="caret"></span>
                      @endif
                    </a>
                    @if( $category->children() )
                    <ul class="dropdown-menu">
                      @foreach( $category->children() as $child)
                        <li><a href="{{ route('category.product.new3', $child->slug) }}">{{ $child->name }}</a></li>
                      @endforeach
                    </ul>
                    @endif
                  </li>
                @endforeach
                </ul>
              </li>
              <li><a href="{{ url('theme3/contact-us') }}">Contact</a></li>
              <li><a href="{{ url('theme3/privacy-policy') }}">Privacy Policy</a></li>
            </ul>
          </div><!--/.nav-collapse -->
        </div>
      </div>       
    </div>
  </section>
  <!-- / menu -->