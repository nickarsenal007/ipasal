<!-- Start Promo section -->
  <section id="aa-promo">
    <div class="container">
      <div class="row">
        <div class="col-md-12">
          <div class="aa-promo-area">
            <div class="row">
              <?php $feat_product = App\Product::where('featured', 1)->first(); ?>
              <!-- promo left -->
              <div class="col-md-5 no-padding">                
                <div class="aa-promo-left">
                  <div class="aa-promo-banner">                    
                    <img src="{{ asset('uploads/products/' . $feat_product->featuredImage) }}" alt="img">                    
                    <div class="aa-prom-content">
                    @foreach($feat_product->tags as $tag)
                      <span>{{ $tag->name }}</span>
                    @endforeach  
                      <h4><a href="{{ route('view.product.new3', $feat_product->slug) }}">{{ $feat_product->productName }}</a></h4>                      
                    </div>
                  </div>
                </div>
              </div>
              <!-- promo right -->
              <div class="col-md-7 no-padding">
                <div class="aa-promo-right">
                @foreach( App\Category::where('featured', 1)->inRandomOrder()->get()->take(4) as $category)
                  <div class="aa-single-promo-right">
                    <div class="aa-promo-banner">                      
                      <img src="{{ asset( $category->image ) }}" alt="img">                      
                      <div class="aa-prom-content">
                        <span>Featured</span>
                        <h4><a href="{{ route('category.product.new3', $category->slug) }}">{{ $category->name }}</a></h4>
                      </div>
                    </div>
                  </div>
                @endforeach
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
  <!-- / Promo section -->