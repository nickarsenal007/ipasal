<!-- popular section -->
  <section id="aa-popular-category">
    <div class="container">
      <div class="row">
        <div class="col-md-12">
          <div class="row">
            <div class="aa-popular-category-area">
              <!-- start prduct navigation -->
             <ul class="nav nav-tabs aa-products-tab">
              <?php $popular = App\Product::productMostBought(8); ?>
              @if( count($popular) )
                <li class="active"><a href="#popular" data-toggle="tab">Popular</a></li>
              @endif
              <?php $featured = App\Product::where('featured', 1)->inRandomOrder()->get()->take(8); ?>
              @if( count($featured) )
                <li><a href="#featured" data-toggle="tab">Featured</a></li>
              @endif
              <?php $sale = App\Product::productSales(8); ?>
              @if( count($sale) )
                <li><a href="#latest" data-toggle="tab">Best Seller</a></li>                    
              @endif
              </ul>
              <!-- Tab panes -->
              <div class="tab-content">
              @if( count($popular) )
                <!-- Start men popular category -->
                <div class="tab-pane fade in active" id="popular">
                  <ul class="aa-product-catg aa-popular-slider">
                  @foreach($popular as $product)
                    <!-- start single product item -->
                    <li>
                      <figure>
                        <a class="aa-product-img" href="{{ route('view.product.new3', $product->slug) }}"><img src="{{ asset('uploads/products/thumbnails/' . $product->featuredImage) }}" alt="{{ $product->productName }}" width="250" height="300"></a>
                        <a class="aa-add-card-btn add-to-cart" title="Add to cart" href="" data-productid="{{$product->id}}" data-product="{{ $product->productName }}" data-rate="{{ $product->rate }}" href="#"><span class="fa fa-shopping-cart"></span>Add To Cart</a>
                         <figcaption>
                          <h4 class="aa-product-title"><a href="{{ route('view.product.new3', $product->slug) }}">{{ $product->productName }}</a></h4>
                          <span class="aa-product-price">{{ $product->rate }}</span>
                          @if( $product->actualRate > $product->rate )
                            <span class="aa-product-price"><del>{{ $product->actualRate }}</del></span>
                          @endif
                        </figcaption>
                      </figure>                     
                      <div class="aa-product-hvr-content">
                        <a href="#" class="add-to-wishlist" data-product="{{ $product->id }}" data-toggle="tooltip" data-placement="top" title="Add to Wishlist"><span class="fa fa-heart-o"></span></a>
                      </div>
                    </li>
                  @endforeach
                  </ul>
                </div>
                <!-- / popular product category -->
              @endif
              @if( count($featured) )
                <!-- start featured product category -->
                <div class="tab-pane fade" id="featured">
                  <ul class="aa-product-catg aa-featured-slider">
                  @foreach($featured as $product)
                    <!-- start single product item -->
                    <li>
                      <figure>
                        <a class="aa-product-img" href="{{ route('view.product.new3', $product->slug) }}"><img src="{{ asset('uploads/products/thumbnails/' . $product->featuredImage) }}" alt="{{ $product->productName }}" width="250" height="300"></a>
                        <a class="aa-add-card-btn add-to-cart" title="Add to cart" href="" data-productid="{{$product->id}}" data-product="{{ $product->productName }}" data-rate="{{ $product->rate }}" href="#"><span class="fa fa-shopping-cart"></span>Add To Cart</a>
                         <figcaption>
                          <h4 class="aa-product-title"><a href="{{ route('view.product.new3', $product->slug) }}">{{ $product->productName }}</a></h4>
                          <span class="aa-product-price">{{ $product->rate }}</span>
                          @if( $product->actualRate > $product->rate )
                            <span class="aa-product-price"><del>{{ $product->actualRate }}</del></span>
                          @endif
                        </figcaption>
                      </figure>                     
                      <div class="aa-product-hvr-content">
                        <a href="#" class="add-to-wishlist" data-product="{{ $product->id }}" data-toggle="tooltip" data-placement="top" title="Add to Wishlist"><span class="fa fa-heart-o"></span></a>
                      </div>
                    </li>
                  @endforeach                                                                             
                  </ul>
                </div>
                <!-- / featured product category -->
              @endif
              @if( count($sale) )
                <!-- start latest product category -->
                <div class="tab-pane fade" id="latest">
                  <ul class="aa-product-catg aa-latest-slider">
                  @foreach($sale as $product)
                    <!-- start single product item -->
                    <li>
                      <figure>
                        <a class="aa-product-img" href="{{ route('view.product.new3', $product->slug) }}"><img src="{{ asset('uploads/products/thumbnails/' . $product->featuredImage) }}" alt="{{ $product->productName }}" width="250" height="300"></a>
                        <a class="aa-add-card-btn add-to-cart" title="Add to cart" href="" data-productid="{{$product->id}}" data-product="{{ $product->productName }}" data-rate="{{ $product->rate }}" href="#"><span class="fa fa-shopping-cart"></span>Add To Cart</a>
                         <figcaption>
                          <h4 class="aa-product-title"><a href="{{ route('view.product.new3', $product->slug) }}">{{ $product->productName }}</a></h4>
                          <span class="aa-product-price">{{ $product->rate }}</span>
                          @if( $product->actualRate > $product->rate )
                            <span class="aa-product-price"><del>{{ $product->actualRate }}</del></span>
                          @endif
                        </figcaption>
                      </figure>                     
                      <div class="aa-product-hvr-content">
                        <a href="#" class="add-to-wishlist" data-product="{{ $product->id }}" data-toggle="tooltip" data-placement="top" title="Add to Wishlist"><span class="fa fa-heart-o"></span></a>
                      </div>
                    </li>
                  @endforeach                                                                                    
                  </ul>
                </div>
                <!-- / latest product category -->              
              @endif
              </div>
            </div>
          </div> 
        </div>
      </div>
    </div>
  </section>
  <!-- / popular section -->