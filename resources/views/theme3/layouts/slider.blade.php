<?php $slides = App\Sliders::inRandomOrder()->get()->take(3); ?>
@if( count($slides) )

<!-- Start slider -->
  <section id="aa-slider">
    <div class="aa-slider-area">
      <div id="sequence" class="seq">
        <div class="seq-screen">
          <ul class="seq-canvas">
          @foreach($slides as $slide)
            <!-- single slide item -->
            <li>
              <div class="seq-model">
                <img data-seq src="{{ 'uploads/sliders/' . $slide->sliderImage }}" alt="slide img" />
              </div>
              <div class="seq-title">
               <span data-seq>{!! $slide->textMain !!}</span>                
                <h2 data-seq>{{ $slide->categoryName }}</h2>                
                <p data-seq>{!! $slide->textSecondary !!}</p>
                <a data-seq href="{{ route('category.product.new3', str_slug($slide->categoryName)) }}" class="aa-shop-now-btn aa-secondary-btn">SHOP NOW</a>
              </div>
            </li>
          @endforeach
          </ul>
        </div>

        @if( count($slides) > 1 )
        <!-- slider navigation btn -->
        <fieldset class="seq-nav" aria-controls="sequence" aria-label="Slider buttons">
          <a type="button" class="seq-prev" aria-label="Previous"><span class="fa fa-angle-left"></span></a>
          <a type="button" class="seq-next" aria-label="Next"><span class="fa fa-angle-right"></span></a>
        </fieldset>
        @endif
      </div>
    </div>
  </section>
  <!-- / slider -->
@endif