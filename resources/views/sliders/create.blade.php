@extends('layouts.app')
@section('content')
<div class="container" style="padding: 25px 15px">
    <h4>Admin Panel</h4>
    <div class="row">
        <div class="col-md-3">
            @include('admin.sidebar')
        </div>
        <div class="col-md-9">
            <div class="card">
              <div class="card-body">
                <div class="card-header">
                    <strong>Create New Slider:</strong>
                </div>
                @include('errors.errors')
                <hr>
                <div class="card-text">
                    <div class="row">
                        <div class="col-md-12">
                            
                        <form action="{{ route('sliders.store') }}" method="POST" enctype="multipart/form-data">
                            {{ csrf_field() }}

                            <div class="form-group">
                                <label>Main Text</label>
                                <textarea name="textMain" rows="3" class="form-control">{{ old('textMain') }}</textarea>
                            </div>

                            <div class="form-group">
                                <label>Secondary Text</label>
                                <input type="text" name="textSecondary" class="form-control" value="{{ old('textSecondary') }}">
                            </div>
                            
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-6">
                                        <label>Slider Image</label>
                                        <input type="file" name="sliderImage" class="form-control">
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-4">
                                        <label>Select Category</label>
                                        <select name="category" class="form-control">
                                        @foreach ( $categories as $category )
                                            <option value="{{ $category->id }}">{{ $category->name }}</option>
                                        @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <input type="submit" name="submit" value="Create Slider" class="btn btn-info">    
                            </div>

                        </form>

                        </div>
                    </div>
                </div>
              </div>
        </div>
        </div>
    </div>
</div>
@endsection
