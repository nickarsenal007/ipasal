@extends('layouts.app')
@section('content')
<div class="container" style="padding: 25px 15px">
    <h4>Admin Panel</h4>
    <div class="row">
        <div class="col-md-3">
            @include('admin.sidebar')
        </div>
        <div class="col-md-9">
            <div class="card">
              <div class="card-body">
                <div class="card-header">
                    <strong>Create New Slider:</strong>
                </div>
                @include('errors.errors')
                <hr>
                <div class="card-text">
                    <div class="row">
                        <div class="col-md-12">
                            
                        <form action="{{ route('sliders.update', $slider->sliderId) }}" method="POST" enctype="multipart/form-data">
                            {{ csrf_field() }}
                            {{ method_field('PUT') }}

                            <div class="form-group">
                                <label>Main Text</label>
                                <textarea name="textMain" rows="3" class="form-control">{{ $slider->textMain }}</textarea>
                            </div>

                            <div class="form-group">
                                <label>Secondary Text</label>
                                <input type="text" name="textSecondary" class="form-control" value="{{ $slider->textSecondary }}">
                            </div>
                            
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-6">
                                        <label>Update Slider Image</label>
                                        <input type="file" name="sliderImage" class="form-control">
                                    </div>
                                    <div class="col-md-4">
                                        <img src="{{ asset( 'uploads/sliders/resized/'.$slider->sliderImage ) }}" width="80" height="50" alt="">
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-4">
                                        <label>Select Category</label>
                                        <select name="category" class="form-control">
                                        @foreach ( $categories as $category )
                                            <option value="{{ $category->id }}" {{ $category->id == $slider->categoryId ? 'selected' : '' }}>{{ $category->name }}</option>
                                        @endforeach
                                        </select>
                                    </div>
                                    <div class="col-md-4">
                                        <label>Show or Hide Slider</label><br>
                                        <input type="radio" name="showSlider" value="1" {{ $slider->showSlider ? 'checked' : '' }}>&nbsp;Show
                                        <input type="radio" name="showSlider" value="0" {{ !$slider->showSlider ? 'checked' : '' }}>&nbsp;Hide
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <input type="submit" name="submit" value="Update Slider" class="btn btn-info">    
                            </div>

                        </form>

                        </div>
                    </div>
                </div>
              </div>
        </div>
        </div>
    </div>
</div>
@endsection
