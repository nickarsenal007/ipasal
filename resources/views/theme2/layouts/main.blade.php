<!DOCTYPE html>
<html lang="en">
<head>
	<!-- Meta Tag -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name='copyright' content=''>
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="csrf-token" content="{{ csrf_token() }}">
	<!-- Title Tag  -->
    <title>Eshop - eCommerce HTML5 Template.</title>
	<!-- Favicon -->
	<link rel="icon" type="image/png" href="{{ asset('themes/2/images/favicon.png') }}">
	<!-- Web Font -->
	<link href="https://fonts.googleapis.com/css?family=Poppins:200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i&display=swap" rel="stylesheet">
	
	<!-- StyleSheet -->
	
	<!-- Bootstrap -->
	<link rel="stylesheet" href="{{ asset('themes/2/css/bootstrap.css') }}">
	<!-- Magnific Popup -->
    <link rel="stylesheet" href="{{ asset('themes/2/css/magnific-popup.min.css') }}">
	<!-- Font Awesome -->
    <link rel="stylesheet" href="{{ asset('themes/2/css/font-awesome.css') }}">
	<!-- Fancybox -->
	<link rel="stylesheet" href="{{ asset('themes/2/css/jquery.fancybox.min.css') }}">
	<!-- Themify Icons -->
    <link rel="stylesheet" href="{{ asset('themes/2/css/themify-icons.css') }}">
	<!-- Nice Select CSS -->
    <link rel="stylesheet" href="{{ asset('themes/2/css/niceselect.css') }}">
	<!-- Animate CSS -->
    <link rel="stylesheet" href="{{ asset('themes/2/css/animate.css') }}">
	<!-- Flex Slider CSS -->
    <link rel="stylesheet" href="{{ asset('themes/2/css/flex-slider.min.css') }}">
	<!-- Owl Carousel -->
    <link rel="stylesheet" href="{{ asset('themes/2/css/owl-carousel.css') }}">
	<!-- Slicknav -->
    <link rel="stylesheet" href="{{ asset('themes/2/css/slicknav.min.css') }}">
	
	<!-- Eshop StyleSheet -->
	<link rel="stylesheet" href="{{ asset('themes/2/css/reset.css') }}">
	<link rel="stylesheet" href="{{ asset('themes/2/style.css') }}">
    <link rel="stylesheet" href="{{ asset('themes/2/css/responsive.css') }}">
	
	@yield('head')

	<style>
		.pagination li{
			display: inline-block;
		    padding: 3px 12px;
		    border: 1px solid;
		}
		.pagination .active{
			background-color: #495057;
			color:white;
			font-weight: 500
		}
		.search-bar-wrapper{
			text-align: center;
			margin-top: 10px;
		}
		.search-bar-wrapper input{
			width: 420px;
		    padding: 12px 10px 12px 10px;
		    border-right: none;
		}
		.search-bar-wrapper .btnn i{
			background-color: #333333;
		    font-weight: 600;
		    color: white;
		    padding: 16px 19px 19px 20px;
		    font-size: 15px;
		    border-top-right-radius: 3px;
		    border-bottom-right-radius: 3px;
		    margin-left: -4px;
		}
	</style>
	
</head>
<body class="js">
	
	@yield('menu')

	@yield('breadcrumbs')

	@yield('content')

	@yield('sliders')

	@yield('productssection')	

	@yield('categorysection')
	
	@yield('popularsection')

	@yield('threecolumns')
	
	<!-- Modal -->
    @include('theme2.layouts.modal')
    <!-- Modal end -->
	
	<!-- Start Footer Area -->
	<footer class="footer">
		<!-- Footer Top -->
		<div class="footer-top section">
			<div class="container">
				<div class="row">
					<div class="col-lg-5 col-md-6 col-12">
						<!-- Single Widget -->
						<div class="single-footer about">
							<div class="logo">
								<a href="{{ url('theme2') }}"><img src="{{ asset('themes/2/images/logo2.png') }}" alt="#"></a>
							</div>
							<p class="text">About Section</p>
							<p class="call">Got Question? Call us 24/7<span><a href="tel:123456789">+ Phone No.</a></span></p>
						</div>
						<!-- End Single Widget -->
					</div>
					<div class="col-lg-2 col-md-6 col-12">
						<!-- Single Widget -->
						<div class="single-footer links">
							<h4>Information</h4>
							<ul>
								<li><a href="#">About Us</a></li>
								<li><a href="{{ url('theme2/privacy-policy') }}">Privacy Policy</a></li>
								<li><a href="{{ url('theme2/contact-us') }}">Contact Us</a></li>
							</ul>
						</div>
						<!-- End Single Widget -->
					</div>
					<div class="col-lg-2 col-md-6 col-12">
						<!-- Single Widget -->
						<div class="single-footer links">
							<h4>Latest Categories</h4>
							<ul>
							@foreach( App\Category::latest()->get()->take(5) as $category )
								<li><a href="{{ route('category.product.new2', $category->slug) }}">{{ $category->name }}</a></li>
							@endforeach
							</ul>
						</div>
						<!-- End Single Widget -->
					</div>
					<div class="col-lg-3 col-md-6 col-12">
						<!-- Single Widget -->
						<div class="single-footer social">
							<h4>Get In Touch</h4>
							<!-- Single Widget -->
							<div class="contact">
								<ul>
									<li>Contacts Here</li>
								</ul>
							</div>
							<!-- End Single Widget -->
							<ul>
								<li><a href="#"><i class="ti-facebook"></i></a></li>
								<li><a href="#"><i class="ti-twitter"></i></a></li>
								<li><a href="#"><i class="ti-flickr"></i></a></li>
								<li><a href="#"><i class="ti-instagram"></i></a></li>
							</ul>
						</div>
						<!-- End Single Widget -->
					</div>
				</div>
			</div>
		</div>
		<!-- End Footer Top -->
		<div class="copyright">
			<div class="container">
				<div class="inner">
					<div class="row">
						<div class="col-lg-6 col-12">
							<div class="left">
								<p>Copyright © 2020 <a href="http://www.wpthemesgrid.com" target="_blank">Wpthemesgrid</a>  -  All Rights Reserved.</p>
							</div>
						</div>
						<div class="col-lg-6 col-12">
							<div class="right">
								<img src="images/payments.png" alt="#">
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</footer>
	<!-- /End Footer Area -->
 
	<!-- Jquery -->
    <script src="{{ asset('themes/2/js/jquery.min.js') }}"></script>
    <script src="{{ asset('themes/2/js/jquery-migrate-3.0.0.js') }}"></script>
	<script src="{{ asset('themes/2/js/jquery-ui.min.js') }}"></script>
	<!-- Popper JS -->
	<script src="{{ asset('themes/2/js/popper.min.js') }}"></script>
	<!-- Bootstrap JS -->
	<script src="{{ asset('themes/2/js/bootstrap.min.js') }}"></script>
	<!-- Color JS -->
	<script src="{{ asset('themes/2/js/colors.js') }}"></script>
	<!-- Slicknav JS -->
	<script src="{{ asset('themes/2/js/slicknav.min.js') }}"></script>
	<!-- Owl Carousel JS -->
	<script src="{{ asset('themes/2/js/owl-carousel.js') }}"></script>
	<!-- Magnific Popup JS -->
	<script src="{{ asset('themes/2/js/magnific-popup.js') }}"></script>
	<!-- Waypoints JS -->
	<script src="{{ asset('themes/2/js/waypoints.min.js') }}"></script>
	<!-- Countdown JS -->
	<script src="{{ asset('themes/2/js/finalcountdown.min.js') }}"></script>
	<!-- Nice Select JS -->
	<script src="{{ asset('themes/2/js/nicesellect.js') }}"></script>
	<!-- Flex Slider JS -->
	<script src="{{ asset('themes/2/js/flex-slider.js') }}"></script>
	<!-- ScrollUp JS -->
	<script src="{{ asset('themes/2/js/scrollup.js') }}"></script>
	<!-- Onepage Nav JS -->
	<script src="{{ asset('themes/2/js/onepage-nav.min.js') }}"></script>
	<!-- Easing JS -->
	<script src="{{ asset('themes/2/js/easing.js') }}"></script>
	<!-- Active JS -->
	<script src="{{ asset('themes/2/js/active.js') }}"></script>
	<!-- Sweet Alert -->
	<script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>

	@yield('footer')

	<script>
		$(document).ready(function(){
	        
	        // Add to Cart 
			$('.add-to-cart').click(function(e){
				e.preventDefault();

				let productId = $(this).data('productid');
				let productName = $(this).data('product');
				let rate = $(this).data('rate');

				$.ajaxSetup({
				  headers: {
				    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
				  }
				});

				$.ajax({
	               type:'POST',
	               url:'{{ url("cart/ajax") }}',
	               data: { productId, productName, rate },
	               success:function(data) {
	               		if ( data.status == 200 ) {
	               			
	               			let count = $('.total-count').text();
	               			let cartcount = parseInt(count) + 1;
	               			$('.total-count').text(cartcount);
	               			$('.cart-count').text(cartcount + ' Item(s)');

	               			Swal.fire({
							  title: 'Success!',
							  text: 'Item added to your cart.',
							  icon: 'success',
							  confirmButtonText: 'Ok'
							})

	               		}else{
	               			
	               			Swal.fire({
							  text: 'Item already on your cart.',
							  icon: 'error',
							  confirmButtonText: 'Exit'
							})
	               		}
	               }
	            });

			});

			// Add to Wish List
			$('.add-to-wishlist').click(function(e){
				e.preventDefault();

				let productId = $(this).data('product');

				$.ajaxSetup({
				  headers: {
				    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
				  }
				});

				$.ajax({
					type:'POST',
					url:'{{ url("wishlist/ajax") }}',
					data: { productId },
					success:function(data) {
						
						if ( data.status == 200 ) {
	               			
	               			Swal.fire({
							  title: 'Success!',
							  text: data.message,
							  icon: 'success',
							  confirmButtonText: 'Ok'
							})

	               		}else{
	               			
	               			Swal.fire({
							  text: 'Please login to add this item in the wish list.',
							  icon: 'error',
							  confirmButtonText: 'Exit'
							})
	               		}

					}
	            });

			});

			// Sort Product
			$('.sort-product').change(function(){

				window.location = `?sort=` + $(this).val();

			});

		});
	</script>
	
</body>
</html>