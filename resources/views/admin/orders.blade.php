@extends('layouts.app')
@section('content')
<div class="container" style="padding: 25px 15px">
    <h5>Admin Panel</h5>
    <div class="row">
    <div class="col-md-3">
        @include('admin.sidebar')
    </div>
    <div class="col-md-9">
        <div class="card">
          <div class="card-body">
            <h5 class="card-title">Products</h5>
            <p class="card-text">
                <table id="example" class="table table-condensed table-responsive">
                    <thead>
                        <tr>
                            <th></th>
                            <th>Item</th>
                            <th>Client Name</th>
                            <th>Email</th>
                            <th>Phone No.</th>
                            <th>Delivery Address</th>
                        </tr>
                    </thead>
                    <tbody>
                    @foreach ($orders as $o)
                        @if($o->product)
                        <?php 
                            $slug = explode(' ', $o->product->title);
                            $slug = implode('-', $slug); 
                        ?>
                          
                        <tr>
                            <td> <img src="{{asset('uploads/products/thumbnails/' . $o->product->image )}}" alt="Alt" height="50" width="50"> </td>
                            <td>{{ $o->product->title }}</td>
                            <td>{{ $o->name }}</td>
                            <td>{{ $o->user_email }}</td>
                            <td>{{ $o->phone }}</td>
                            <td>{{ $o->address }}</td>
                        </tr>
                        @endif
                    @endforeach
                    </tbody>
                </table>
            </p>
          </div>
        </div>
    </div>
    </div>
</div>
@endsection
