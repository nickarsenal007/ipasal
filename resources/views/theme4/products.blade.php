@extends('theme4.layouts.main')

@section('content')

	<!-- Start All Title Box -->
    <div class="all-title-box">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <h2>{{ $title }}</h2>
                    <ul class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{ url('theme4') }}">Home</a></li>
                        <li class="breadcrumb-item active">{{ $catname->name }}</li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <!-- End All Title Box -->

    <!-- Start Shop Page  -->
    <div class="shop-box-inner">
        <div class="container">
            <div class="row">
                <div class="col-xl-3 col-lg-3 col-sm-12 col-xs-12 sidebar-shop-left">
                    <div class="product-categori">
                        <div class="search-product">
                            <form action="#">
                                <input class="form-control" placeholder="Search here..." type="text">
                                <button type="submit"> <i class="fa fa-search"></i> </button>
                            </form>
                        </div>
                        <div class="filter-sidebar-left">
                            <div class="title-left">
                                <h3>Categories</h3>
                            </div>
                            @if( count($parentcategories) == 0 )
                            	<?php $parentcategories = $categories; ?>
                            @endif
                            @foreach($parentcategories as $category)
                            <div class="list-group list-group-collapse list-group-sm list-group-tree" id="list-group-men" data-children=".sub-men">
                                <div class="list-group-collapse sub-men">
                                    <a class="list-group-item list-group-item-action" href="#sub-men1" data-toggle="collapse" aria-expanded="true" aria-controls="sub-men1">{{ $category->name }}
								</a>
								@if( $category->children() )
                                    <div class="collapse show" id="sub-men1" data-parent="#list-group-men">
                                        <div class="list-group">
                                        @foreach( $category->children() as $cat)
                                            <a href="{{ route('category.product.new4', $cat->slug) }}" class="list-group-item list-group-item-action active">{{ $cat->name }} <small class="text-muted">({{ $cat->products()->count() }})</small></a>
                                        @endforeach
                                        </div>
                                    </div>
                                @endif
                                </div>
                            </div>
                            @endforeach
                        </div>
                    </div>
                </div>
                <div class="col-xl-9 col-lg-9 col-sm-12 col-xs-12 shop-content-right">
                    <div class="right-product-box">
                        <div class="product-item-filter row">
                            <div class="col-12 col-sm-8 text-center text-sm-left">
                                <div class="toolbar-sorter-right">
                                    <span>Sort by </span>
                                    <select id="basic" class="selectpicker show-tick form-control sort-product" data-placeholder="$ USD">
									<option>Name</option>
									<option value="high_low" {{ request()->sort == 'high_low' ? 'selected' : '' }}>High Price → Low Price</option>
									<option value="low_high" {{ request()->sort == 'low_high' ? 'selected' : '' }}>Low Price → High Price</option>
								</select>
                                </div>
                            </div>
                        </div>

                        <div class="row product-categorie-box">
                            <div class="tab-content">
                                <div role="tabpanel" class="tab-pane fade show active" id="grid-view">
                                    <div class="row">
                                    @if( count($products) )    
                                   	@foreach($products as $product)
                                        <div class="col-sm-6 col-md-6 col-lg-4 col-xl-4">
                                            <div class="products-single fix">
                                                <div class="box-img-hover">
                                                @if( $product->discountPercent )
                                                    <div class="type-lb">
                                                        <p class="sale">{{ $product->discountPercent }}% Off</p>
                                                    </div>
                                                @endif
                                                    <img src="{{ asset('uploads/products/thumbnails/' . $product->featuredImage) }}" class="img-fluid" width="100%" alt="Image">
                                                    <div class="mask-icon">
                                                        <ul>
                                                            <li><a href="{{ route('view.product.new4', $product->slug) }}" data-toggle="tooltip" data-placement="right" title="View"><i class="fas fa-eye"></i></a></li>
                                                            <li><a href="#" class="add-to-wishlist" data-product="{{ $product->id }}" data-toggle="tooltip" data-placement="right" title="Add to Wishlist"><i class="far fa-heart"></i></a></li>
                                                        </ul>
                                                        <a class="cart add-to-cart" title="Add to cart" href="" data-productid="{{$product->id}}" data-product="{{ $product->productName }}" data-rate="{{ $product->rate }}" href="#">Add to Cart</a>
                                                    </div>
                                                </div>
                                                <div class="why-text">
                                                    <a href="{{ route('view.product.new4', $product->slug) }}">
                                                    	<h4>{{ $product->productName }}</h4>
                                                    </a>
                                                    <h5>NRs. {{ $product->rate }}</h5>
                                                </div>
                                            </div>
                                        </div>
                                    @endforeach
                                    @else
                                    <div class="col-12">
                                    	No products at the moment.
                                    </div>
                                    @endif
                                    </div>
                                </div>
                            </div>
                        </div>
                        <style>
                            .pagination li{
                                font-size: 15px;
                                margin-right: 5px;
                                border: 1px solid #ccc;
                                padding: 0px 10px;
                            }
                            .pagination .active{
                                color: white;
                                font-weight: 500;
                                background-color: #202020;    
                            }
                        </style>
                        {{ $products->links() }}

                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- End Shop Page -->

@endsection