@extends('theme4.layouts.main')

@section('slider')

	@include('theme4.layouts.slider')

@endsection

@section('categories')

	@include('theme4.layouts.category')

@endsection

@section('promo')

	@include('theme4.layouts.promo')

@endsection

@section('products')

	@include('theme4.layouts.popular')

@endsection
