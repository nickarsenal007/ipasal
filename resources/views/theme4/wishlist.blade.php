@extends('theme4.layouts.main')

@section('content')

<!-- Start All Title Box -->
    <div class="all-title-box">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <h2>{{ $title }}</h2>
                    <ul class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{ url('theme4') }}">Home</a></li>
                        <li class="breadcrumb-item active">{{ $title }}</li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
<!-- End All Title Box -->

<!-- Start Wishlist  -->
    <div class="wishlist-box-main">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="table-main table-responsive">
                        <table class="table">
                            <thead>
                                <tr>
                                    <th>Images</th>
                                    <th>Product Name</th>
                                    <th>Unit Price </th>
                                    <th>Stock</th>
                                    <th></th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                            @if( count($products) )
                            @foreach($products as $product)
                                <tr>
                                    <td class="thumbnail-img">
                                        <a href="#">
                                    <img class="img-fluid" src="{{ asset('uploads/products/thumbnails/'. $product->featuredImage) }}" alt="" />
                                </a>
                                    </td>
                                    <td class="name-pr">
                                        <a href="{{ route('view.product.new4', $product->slug) }}">
                                        {{ $product->productName }}
                                        </a>
                                    </td>
                                    <td class="price-pr">
                                        <p>NRs. {{ $product->rate }}</p>
                                    </td>
                                    <td class="quantity-box">In Stock</td>
                                    <td class="add-pr">
                                        <a class="btn hvr-hover add-to-cart" title="Add to cart" href="" data-productid="{{$product->id}}" data-product="{{ $product->productName }}" data-rate="{{ $product->rate }}" href="#"><i class="fa fa-cart-plus"></i></a>
                                    </td>
                                    <td>
                                        <a class="btn hvr-hover add-to-wishlist" data-product="{{ $product->id }}" href="" style="color: white"><i class="fa fa-heart"></i></a>
                                    </td>
                                </tr>
                            @endforeach
                            @else
                                <tr>
                                    <td colspan="5">No products at the moment.</td>
                                </tr>
                            @endif
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- End Wishlist -->

@endsection