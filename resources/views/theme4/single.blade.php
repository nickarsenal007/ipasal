@extends('theme4.layouts.main')

@section('content')


<!-- Start All Title Box -->
    <div class="all-title-box">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <h2>{{ $product->productName }}</h2>
                    <ul class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{ url('theme4') }}">Home</a></li>
                        <li class="breadcrumb-item active">{{ $category->name }}</li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <!-- End All Title Box -->

    <!-- Start Shop Detail  -->
    <div class="shop-detail-box-main">
        <div class="container">
            <div class="row">
                <div class="col-xl-5 col-lg-5 col-md-6">
                    <div id="carousel-example-1" class="single-product-slider carousel slide" data-ride="carousel">
                        <div class="carousel-inner" role="listbox">
                            <div class="carousel-item active"> <img class="d-block w-100" src="{{  asset('uploads/products/'.$product->featuredImage) }}" alt="First slide"> </div>
                            @foreach( $product->images as $image )
                            <div class="carousel-item"> <img class="d-block w-100" src="{{  asset('uploads/products/'.$image->image) }}" alt="slide"> </div>
                            @endforeach
                        </div>
                        @if( $product->images()->count() > 0 )
                        <a class="carousel-control-prev" href="#carousel-example-1" role="button" data-slide="prev"> 
						<i class="fa fa-angle-left" aria-hidden="true"></i>
						<span class="sr-only">Previous</span> 
						</a>
                        <a class="carousel-control-next" href="#carousel-example-1" role="button" data-slide="next"> 
						<i class="fa fa-angle-right" aria-hidden="true"></i> 
						<span class="sr-only">Next</span> 
						</a>
						@endif
                    </div>
                </div>
                <div class="col-xl-7 col-lg-7 col-md-6">
                    <div class="single-product-details">
                        <h2>{{ $product->productName }}</h2>
                        <h5> @if($product->rate < $product->actualRate) <del>NRs. {{ $product->actualRate }}</del> @endif NRs. {{ $product->rate }}</h5>
                        <p class="available-stock">
                        	<style>
		                      	.yellow{ color: orange }
							</style>
							@if( $averageRating > 0 )
							<i class="{{ $averageRating >= 1 ? 'yellow' : '' }} fa fa-star"></i>
							<i class="{{ $averageRating >= 2 ? 'yellow' : '' }} fa fa-star"></i>
							<i class="{{ $averageRating >= 3 ? 'yellow' : '' }} fa fa-star"></i>
							<i class="{{ $averageRating >= 4 ? 'yellow' : '' }} fa fa-star"></i>
							<i class="{{ $averageRating >= 5 ? 'yellow' : '' }} fa fa-star"></i>
							@endif
                        	<span><a href="#">{{ $product->reviews()->count() }} Review(s) </a></span>
                        </p>
                            <p>
                                <h4>Short Description:</h4>
                                <p>{{ $product->shortDesc }}</p>
								
								<h4>More Details:</h4>
                                <p>{!! $product->description !!}</p>

                                <div class="price-box-bar">
                                    <div class="cart-and-bay-btn">
                                        <a class="btn hvr-hover add-to-cart" data-fancybox-close="" data-productid="{{ $product->id }}" data-product="{{ $product->productName }}" data-rate="{{ $product->rate }}" href="#">Add to cart</a>
                                        <a class="btn hvr-hover add-to-wishlist" data-product="{{ $product->id }}" href="#"><i class="fas fa-heart"></i> Add to wishlist</a>
                                    </div>
                                </div>

                                <div class="add-to-btn">
                                    <!-- <div class="add-comp">
                                        
                                    </div> -->
                                    <!-- <div class="share-bar">
                                        <a class="btn hvr-hover" href="#"><i class="fab fa-facebook" aria-hidden="true"></i></a>
                                        <a class="btn hvr-hover" href="#"><i class="fab fa-google-plus" aria-hidden="true"></i></a>
                                        <a class="btn hvr-hover" href="#"><i class="fab fa-twitter" aria-hidden="true"></i></a>
                                        <a class="btn hvr-hover" href="#"><i class="fab fa-pinterest-p" aria-hidden="true"></i></a>
                                        <a class="btn hvr-hover" href="#"><i class="fab fa-whatsapp" aria-hidden="true"></i></a>
                                    </div> -->
                                </div>
                    </div>
                </div>
            </div>
			@if( count( $relatedproducts ) )
            <div class="row my-5">
                <div class="col-lg-12">
                    <div class="title-all text-center">
                        <h1>Related Products</h1>
                        <hr>
                    </div>
                    <div class="">
                    @foreach( $relatedproducts as $prod )
                        <div class="item col-md-3">
                            <div class="products-single fix">
                                <div class="box-img-hover">
                                    <img src="{{  asset('uploads/products/thumbnails/'.$prod->featuredImage) }}" width="100%" class="img-fluid" alt="Image">
                                    <div class="mask-icon">
                                        <ul>
                                            <li><a href="{{ route('view.product.new4', $prod->slug) }}" data-toggle="tooltip" data-placement="right" title="View"><i class="fas fa-eye"></i></a></li>
                                            <li><a href="#" class="add-to-wishlist" data-product="{{ $prod->id }}" data-toggle="tooltip" data-placement="right" title="Add to Wishlist"><i class="far fa-heart"></i></a></li>
                                        </ul>
                                        <a class="cart add-to-cart" href="#" data-productid="{{ $prod->id }}" data-product="{{ $prod->productName }}" data-rate="{{ $prod->rate }}">Add to Cart</a>
                                    </div>
                                </div>
                                <div class="why-text">
                                    <a href="{{ route('view.product.new4', $prod->slug) }}"><h4>{{ $prod->productName }}</h4></a>
                                    <h5> NRs. {{ $prod->rate }}</h5>
                                </div>
                            </div>
                        </div>
					@endforeach
                    </div>
                </div>
            </div>
			@endif
        </div>
    </div>
    <!-- End Cart -->


@endsection

@section('footer')

<script src="{{ asset('js/jquery.emojiRatings.min.js') }}"></script>
<script defer src="{{ asset('js/jquery.elevateZoom-3.0.8.min.js') }}"></script>
<script type="text/javascript">
    function change_image(x){
        var nn =x.src ;
        document.getElementById("myImg").src = nn;
        document.getElementById("demo").src = nn;
    }
</script>
<script type="text/javascript">
    $(document).ready(function() {
        $("#zoom_10").elevateZoom({
            easing : true,
            zoomWindowWidth:350,
            zoomWindowHeight:350,
            zoomWindowPosition: 1, zoomWindowOffetx: 250
        });
    });
    $("#rating").emojiRating({
                fontSize: 20,
                onUpdate: function(count) {
                    $(".review-text").show();
                    $("#starCount").html(count + " Star");
                    var rating = $('.emoji-rating').val();
                    $('#rating-input').val(rating);
                }
            });


            $('#order-submit').click(function(e){
                $(this).prop('disabled', true);
                $('#oform').submit();
            });
</script>

@endsection