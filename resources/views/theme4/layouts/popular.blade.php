<?php $recentproducts = App\Product::orderBy('created_at')->get()->take(6); ?>
@if( count($recentproducts) )
<!-- Start Blog  -->
    <div class="latest-blog">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="title-all text-center">
                        <h1>Recent Products</h1>
                        <p>Check out our latest and popular products.</p>
                    </div>
                </div>
            </div>
            <div class="row">
            @foreach( $recentproducts as $product )
                <div class="col-md-6 col-lg-4 col-xl-4">
                    <div class="blog-box">
                        <div class="blog-img">
                            <img class="img-fluid" width="100%" src="{{ asset('uploads/products/thumbnails/' . $product->featuredImage) }}" alt="" />
                        </div>
                        <div class="blog-content">
                            <div class="title-blog">
                                <h3>{{ $product->productName }}</h3>
                                <h4><b>NRs. {{ $product->rate }}</b></h4>
                            </div>
                            <ul class="option-blog">
                                <li><a href="#" class="add-to-wishlist" data-product="{{ $product->id }}" data-toggle="tooltip" data-placement="right" title="Add to Wishlist"><i class="far fa-heart"></i></a></li>
                                <li><a href="{{ route('view.product.new4', $product->slug) }}" data-toggle="tooltip" data-placement="right" title="View Product"><i class="fas fa-eye"></i></a></li>
                                <li><a class="add-to-cart" data-toggle="tooltip" data-placement="right" title="Add to Cart" data-productid="{{$product->id}}" data-product="{{ $product->productName }}" data-rate="{{ $product->rate }}" href="#"><i class="fas fa-cart-plus"></i></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            @endforeach
            </div>
        </div>
    </div>
    <!-- End Blog  -->
@endif