<?php $featuredCategories = App\Category::where('featured', 1)->inRandomOrder()->get()->take(6); ?>
@if( count($featuredCategories) )
<!-- Start Categories  -->
    <div class="categories-shop">
        <div class="container">
            <div class="row">
            @foreach($featuredCategories as $category)
                <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                    <div class="shop-cat-box">
                        <img class="img-fluid" src="{{ asset( $category->image ) }}" alt="" />
                        <a class="btn hvr-hover" href="{{ route('category.product.new4', $category->slug) }}">{{ $category->name }}</a>
                    </div>
                </div>
            @endforeach
            </div>
        </div>
    </div>
    <!-- End Categories -->
@endif