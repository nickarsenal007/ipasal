<!DOCTYPE html>
<html lang="en">
<!-- Basic -->

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <!-- Mobile Metas -->
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Site Metas -->
    <title>ThewayShop - Ecommerce Bootstrap 4 HTML Template</title>
    <meta name="keywords" content="">
    <meta name="description" content="">
    <meta name="author" content="">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    
    <!-- Site Icons -->
    <link rel="shortcut icon" href="{{ asset('themes/4/images/favicon.ico') }}" type="image/x-icon">
    <link rel="apple-touch-icon" href="{{ asset('themes/4/images/apple-touch-icon.png') }}">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="{{ asset('themes/4/css/bootstrap.min.css') }}">
    <!-- Site CSS -->
    <link rel="stylesheet" href="{{ asset('themes/4/css/style.css') }}">
    <!-- Responsive CSS -->
    <link rel="stylesheet" href="{{ asset('themes/4/css/responsive.css') }}">
    <!-- Custom CSS -->
    <link rel="stylesheet" href="{{ asset('themes/4/css/custom.css') }}">

    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body>

    @include('theme4.layouts.head')

    @include('theme4.layouts.menu')

    @yield('slider')

    @yield('content')

    @yield('categories')

    @yield('promo')

    @yield('products')

    <!-- Start Footer  -->
    <footer>
        <div class="footer-main">
            <div class="container">
                <div class="row">
                    <div class="col-lg-4 col-md-12 col-sm-12">
                        <div class="footer-widget">
                            <h4>About ThewayShop</h4>
                            <p>-- About --</p>
                            <ul>
                                <li><a href="#"><i class="fab fa-facebook" aria-hidden="true"></i></a></li>
                                <li><a href="#"><i class="fab fa-twitter" aria-hidden="true"></i></a></li>
                                <li><a href="#"><i class="fab fa-linkedin" aria-hidden="true"></i></a></li>
                                <li><a href="#"><i class="fab fa-google-plus" aria-hidden="true"></i></a></li>
                                <li><a href="#"><i class="fa fa-rss" aria-hidden="true"></i></a></li>
                                <li><a href="#"><i class="fab fa-pinterest-p" aria-hidden="true"></i></a></li>
                                <li><a href="#"><i class="fab fa-whatsapp" aria-hidden="true"></i></a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-12 col-sm-12">
                        <div class="footer-link">
                            <h4>Information</h4>
                            <ul>
                                <li><a href="#">About Us</a></li>
                                <li><a href="#">Privacy Policy</a></li>
                                <li><a href="#">Contact Us</a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-12 col-sm-12">
                        <div class="footer-link-contact">
                            <h4>Contact Us</h4>
                            <ul>
                                <li>
                                    <p><i class="fas fa-map-marker-alt"></i>Address: - </p>
                                </li>
                                <li>
                                    <p><i class="fas fa-phone-square"></i>Phone: <a href="tel:+1-888705770">+ -</a></p>
                                </li>
                                <li>
                                    <p><i class="fas fa-envelope"></i>Email: <a href="mailto:contactinfo@gmail.com">contactinfo@gmail.com</a></p>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </footer>
    <!-- End Footer  -->

    <!-- Start copyright  -->
    <div class="footer-copyright">
        <p class="footer-company">All Rights Reserved. &copy; 2018 <a href="#">ThewayShop</a> Design By :
            <a href="https://html.design/">html design</a></p>
    </div>
    <!-- End copyright  -->

    <a href="#" id="back-to-top" title="Back to top" style="display: none;">&uarr;</a>

    <!-- ALL JS FILES -->
    <script src="{{ asset('themes/4/js/jquery-3.2.1.min.js') }}"></script>
    <script src="{{ asset('themes/4/js/popper.min.js') }}"></script>
    <script src="{{ asset('themes/4/js/bootstrap.min.js') }}"></script>
    <!-- ALL PLUGINS -->
    <script src="{{ asset('themes/4/js/jquery.superslides.min.js') }}"></script>
    <script src="{{ asset('themes/4/js/bootstrap-select.js') }}"></script>
    <script src="{{ asset('themes/4/js/inewsticker.js') }}"></script>
    <script src="{{ asset('themes/4/js/bootsnav.js') }}"></script>
    <script src="{{ asset('themes/4/js/images-loded.min.js') }}"></script>
    <script src="{{ asset('themes/4/js/isotope.min.js') }}"></script>
    <script src="{{ asset('themes/4/js/owl.carousel.min.js') }}"></script>
    <script src="{{ asset('themes/4/js/baguetteBox.min.js') }}"></script>
    <script src="{{ asset('themes/4/js/form-validator.min.js') }}"></script>
    <script src="{{ asset('themes/4/js/contact-form-script.js') }}"></script>
    <script src="{{ asset('themes/4/js/custom.js') }}"></script>

    <!-- Sweet Alert -->
  <script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>
  @yield('footer')

  <script>
    $(document).ready(function(){
          
          // Add to Cart 
      $('.add-to-cart').click(function(e){
        e.preventDefault();

        let productId = $(this).data('productid');
        let productName = $(this).data('product');
        let rate = $(this).data('rate');

        $.ajaxSetup({
          headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
          }
        });

        $.ajax({
                 type:'POST',
                 url:'{{ url("cart/ajax") }}',
                 data: { productId, productName, rate },
                 success:function(data) {
                  console.log(data);
                    if ( data.status == 200 ) {
                      
                      let count = $('.total-count').text();
                      let cartcount = parseInt(count) + 1;
                      $('.total-count').text(cartcount);
                      $('.cart-count').text(cartcount + ' Item(s)');

                      Swal.fire({
                        title: 'Success!',
                        text: 'Item added to your cart.',
                        icon: 'success',
                        confirmButtonText: 'Ok'
                      })

                    }else{
                      
                      Swal.fire({
                        text: 'Item already on your cart.',
                        icon: 'error',
                        confirmButtonText: 'Exit'
                      })
                    }
                 }
              });

      });

      // Add to Wish List
      $('.add-to-wishlist').click(function(e){
        e.preventDefault();

        let productId = $(this).data('product');

        $.ajaxSetup({
          headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
          }
        });

        $.ajax({
          type:'POST',
          url:'{{ url("wishlist/ajax") }}',
          data: { productId },
          success:function(data) {
            
            if ( data.status == 200 ) {
                      
                      
                      Swal.fire({
                        title: 'Success!',
                        text: data.message,
                        icon: 'success',
                        confirmButtonText: 'Ok'
                      })

                    }else{
                      
                      Swal.fire({
                        text: 'Please login to add this item in the wish list.',
                        icon: 'error',
                        confirmButtonText: 'Exit'
                      })
                      
                    }

          }
              });

      });

      // Sort Product
      $('.sort-product').change(function(){

        window.location = `?sort=` + $(this).val();

      });

    });
  </script>
  
</body>

</html>