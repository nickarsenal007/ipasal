<?php $slides = App\Sliders::inRandomOrder()->get()->take(3); ?>
@if( count($slides) )
<!-- Start Slider -->
    <div id="slides-shop" class="cover-slides">
        <ul class="slides-container">
            <!-- text-left / text-center / text-right -->
            @foreach($slides as $slide)
            <li class="text-left">
                <img src="{{ 'uploads/sliders/' . $slide->sliderImage }}" alt="slide img">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <h1 class="m-b-20">{!! $slide->textMain !!}</h1>
                            <p class="m-b-40">{!! $slide->textSecondary !!}</p>
                            <p><a class="btn hvr-hover" href="{{ route('category.product.new4', str_slug($slide->categoryName)) }}">Shop New</a></p>
                        </div>
                    </div>
                </div>
            </li>
            @endforeach
        </ul>
        @if( count($slides) > 1 )
        <div class="slides-navigation">
            <a href="#" class="next"><i class="fa fa-angle-right" aria-hidden="true"></i></a>
            <a href="#" class="prev"><i class="fa fa-angle-left" aria-hidden="true"></i></a>
        </div>
        @endif
    </div>
    <!-- End Slider -->
@endif