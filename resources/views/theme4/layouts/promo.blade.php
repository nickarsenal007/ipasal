<!-- Start Products  -->
    <div class="products-box">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="title-all text-center">
                        <h1>Featured Products</h1>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <div class="special-menu text-center">
                        <div class="button-group filter-button-group">
                            <button class="active" data-filter="*">All</button>
                        <?php $featured = App\Product::where('featured', 1)->inRandomOrder()->get()->take(8); ?>
                        @if( count($featured) )
                            <button data-filter=".top-featured">Top featured</button>
                        @endif
                        <?php $sale = App\Product::productSales(8); ?>
                        @if( count($sale) )
                            <button data-filter=".best-seller">Best seller</button>
                        @endif
                        </div>
                    </div>
                </div>
            </div>
            <div class="row special-list">
                @if( count($featured) )
                @foreach($featured as $product)
                <div class="col-lg-3 col-md-3 col-sm-6 special-grid top-featured">
                    <div class="products-single fix">
                        <div class="box-img-hover">
                            @if( $product->discountPercent )
                            <div class="type-lb">
                                <p class="new">{{ $product->discountPercent }}% Off</p>
                            </div>
                            @endif
                            <img src="{{ asset('uploads/products/thumbnails/' . $product->featuredImage) }}" class="img-fluid" alt="Image">
                            <div class="mask-icon">
                                <ul>
                                    <li><a href="{{ route('view.product.new4', $product->slug) }}" data-toggle="tooltip" data-placement="right" title="View"><i class="fas fa-eye"></i></a></li>
                                    <li><a class="add-to-wishlist" data-product="{{ $product->id }}" href="#" data-toggle="tooltip" data-placement="right" title="Add to Wishlist"><i class="far fa-heart"></i></a></li>
                                </ul>
                                <a class="cart add-to-cart" data-productid="{{$product->id}}" data-product="{{ $product->productName }}" data-rate="{{ $product->rate }}" href="#">Add to Cart</a>
                            </div>
                        </div>
                        <div class="why-text">
                            <a href="{{ route('view.product.new4', $product->slug) }}">
                                <h4>{{ $product->productName }}</h4>
                            </a>
                            <h5>NRs. {{ $product->rate }}</h5>
                        </div>
                    </div>
                </div>
                @endforeach
                @endif
                @if( count($sale) )
                @foreach($featured as $product)
                <div class="col-lg-3 col-md-3 col-sm-6 special-grid best-seller">
                    <div class="products-single fix">
                        <div class="box-img-hover">
                            @if( $product->discountPercent )
                            <div class="type-lb">
                                <p class="new">{{ $product->discountPercent }}% Off</p>
                            </div>
                            @endif
                            <img src="{{ asset('uploads/products/thumbnails/' . $product->featuredImage) }}" class="img-fluid" alt="Image">
                            <div class="mask-icon">
                                <ul>
                                    <li><a href="{{ route('view.product.new4', $product->slug) }}" data-toggle="tooltip" data-placement="right" title="View"><i class="fas fa-eye"></i></a></li>
                                    <li><a class="add-to-wishlist" data-product="{{ $product->id }}" href="#" data-toggle="tooltip" data-placement="right" title="Add to Wishlist"><i class="far fa-heart"></i></a></li>
                                </ul>
                                <a class="cart" href="#">Add to Cart</a>
                            </div>
                        </div>
                        <div class="why-text">
                            <a href="{{ route('view.product.new4', $product->slug) }}">
                                <h4>{{ $product->productName }}</h4>
                            </a>
                            <h5>NRs. {{ $product->rate }}</h5>
                        </div>
                    </div>
                </div>
                @endforeach
                @endif 
            </div>
        </div>
    </div>
    <!-- End Products  -->