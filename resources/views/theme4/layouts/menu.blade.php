<!-- Start Main Top -->
    <header class="main-header">
        <!-- Start Navigation -->
        <nav class="navbar navbar-expand-lg navbar-light bg-light navbar-default bootsnav">
            <div class="container">
                <!-- Start Header Navigation -->
                <div class="navbar-header">
                    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbar-menu" aria-controls="navbars-rs-food" aria-expanded="false" aria-label="Toggle navigation">
                    <i class="fa fa-bars"></i>
                </button>
                    <a class="navbar-brand" href="{{ url('theme4') }}"><img src="{{ asset('themes/4/images/logo.png') }}" class="logo" alt=""></a>
                </div>
                <!-- End Header Navigation -->

                <!-- Collect the nav links, forms, and other content for toggling -->
                <div class="collapse navbar-collapse" id="navbar-menu">
                    <ul class="nav navbar-nav ml-auto" data-in="fadeInDown" data-out="fadeOutUp">
                        <li class="nav-item active"><a class="nav-link" href="{{ url('theme4') }}">Home</a></li>
                        <li class="nav-item"><a class="nav-link" href="{{ url('theme4/about-us') }}">About Us</a></li>
                        <?php $categories = App\Category::where('parentId', 0 )->get()->take(4); ?>
                        @if( count($categories) )
                        <li class="dropdown megamenu-fw">
                            <a href="#" class="nav-link dropdown-toggle" data-toggle="dropdown">Categories</a>
                            <ul class="dropdown-menu megamenu-content" role="menu">
                                <li>
                                    <div class="row">
                                    @foreach($categories as $category)
                                        <div class="col-menu col-md-3">
                                            <h6 class="title">{{ $category->name }}</h6>
                                            @if( $category->children() )
                                            <div class="content">
                                                <ul class="menu-col">
                                                @foreach( $category->children()->take(4) as $child)
                                                    <li><a href="{{ route('category.product.new4', $child->slug) }}">{{ $child->name }}</a></li>
                                                @endforeach
                                                </ul>
                                            </div>
                                            @endif
                                        </div>
                                        <!-- end col-3 -->
                                    @endforeach
                                    </div>
                                    <!-- end row -->
                                </li>
                            </ul>
                        </li>
                        @endif
                        <li class="dropdown">
                            <a href="#" class="nav-link dropdown-toggle" data-toggle="dropdown">SHOP</a>
                            <ul class="dropdown-menu">
                                <li><a href="{{ route('cart.view4') }}">Cart</a></li>
                                <li><a href="{{ route('checkout.view4') }}">Checkout</a></li>
                                @auth
                                <li><a href="{{ route('wish4', Auth::id()) }}">Wishlist</a></li>
                                @endauth
                            </ul>
                        </li>
                        <li class="nav-item"><a class="nav-link" href="{{ url('theme4/contact-us') }}">Contact Us</a></li>
                    </ul>
                </div>
                <!-- /.navbar-collapse -->

                <!-- Start Atribute Navigation -->
                <div class="attr-nav">
                    <ul>
                        <li class="search"><a href="#"><i class="fa fa-search"></i></a></li>
                        
                            <a href="{{ route('cart.view4') }}">
                                <i class="fa fa-shopping-bag"></i>
                                <span class="badge">{{ Cart::content()->count() }}</span>
					       </a>
                        
                    </ul>
                </div>
                <!-- End Atribute Navigation -->
            </div>
        </nav>
        <!-- End Navigation -->
    </header>
    <!-- End Main Top -->

    <!-- Start Top Search -->
    <div class="top-search">
        <div class="container">
            <form action="{{ route('search.product4') }}" method="post">
                {{ csrf_field() }}
                <div class="input-group">
                    <span class="input-group-addon"><i class="fa fa-search"></i></span>
                    <input type="text" name="search" class="form-control" placeholder="Type and enter">
                    <span class="input-group-addon close-search"><i class="fa fa-times"></i></span>
                </div>
            </form>
        </div>
    </div>
    <!-- End Top Search -->